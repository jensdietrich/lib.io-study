package libiostudy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

public class GenerateAdaptionStats {
    //public static Logger LOGGER = Logging.getLogger("compute-adaption-stats");
    public static File FIRST_LAST_DATA_FOLDER = new File("out/");
    public static final File RESULT_FOLDER = new File("out/");
    public static final File STATS_CSV = new File(RESULT_FOLDER,"adaption-stats.csv");
    public static final File STATS_LATEX = new File(RESULT_FOLDER,"adaption-stats.tex");
    public static final String[] TOOLING_LABELS = {"EXPLICIT_STATEMENT","DEFAULT_TOOLING","OTHER_TOOLING"};
	public static final String[] CLASSIFICATIONS = strip(Classifications.ALL.toArray(new String[0]), "unresolved",
			"unclassified");

    public static void main (String[] args) throws Exception {

        long t1 = System.currentTimeMillis();

        String tmp = null;
        if (args.length==1) {
            tmp = args[0];
            Preconditions.checkArgument(PackageManagers.ALL.contains(tmp),"This is not the name of a package manager: " + tmp);
        }
        final String pckManager = tmp;
        Set<String> packageManagers = pckManager==null ? PackageManagers.ALL : Sets.newHashSet(pckManager);

        // read rules
        File ruleFolder = new File(new File(System.getProperty("user.dir")).getParent(),"mapping-rules");
        // parse rules
        HashMap<String,List<Rule>> rules = new HashMap<>();
        for(String packageManager : packageManagers) {
        		rules.put(packageManager,new RuleParser().parse(new File(ruleFolder, packageManager + ".rules"),false));
        }
        // print headers
        for(String label : TOOLING_LABELS) {
        		System.out.print(" & ");
        		System.out.print(label);
        }
        for(String classification : CLASSIFICATIONS) {
        		System.out.print("& " + classification + " ");
        }
        System.out.println(" & Total\\\\");
        System.out.println("\\hline");
        System.out.println("\\hline");
        // print data directly as latex
        String[] managers = packageManagers.toArray(new String[packageManagers.size()]);
        Arrays.sort(managers);
        for(String packageManager : managers) {
    			System.out.print(packageManager);
        		HashMap<String,String> tooling = classifyTooling(new File(ruleFolder, packageManager + ".rules"));
        		for(String label : TOOLING_LABELS) {
        			System.out.print(" & ");
        			System.out.print(tooling.get(label));
        		}
        		Set<String> supported = determineSupportedClassifications(rules.get(packageManager));
        		int count = 0;
        		for(String classification : CLASSIFICATIONS) {
        			if(supported.contains(classification)) {
        				System.out.print(" & \\checkmark");
        				count ++;
        			} else {
        				System.out.print(" & \\xmark");
        			}
        		}
        		System.out.print(" & " + count);
        		System.out.println("\\\\");
        		System.out.println("\\hline");
        }
    }

    public static Set<String> determineSupportedClassifications(List<Rule> rules) {
    		HashSet<String> results = new HashSet<>();
    		for(Rule r : rules) {
    			results.add(r.getClassification());
    		}
    		return results;
    }

    public static HashMap<String,String> classifyTooling(File file) throws IOException {
    		HashMap<String,String> results = new HashMap<>();
    		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
    		    String line;
    		    while ((line = br.readLine()) != null) {
    		    		// process the line.
    	    			for(String label : TOOLING_LABELS) {
    	    				String prefix = "# " + label;
    	    				if(line.startsWith(prefix)) {
    	    					line = line.replace(prefix, "");
    	    					line = line.replace(" ","");
    	    					results.put(label, line);
    	    					break;
					}
    	    			}
    		    }
    		}
    		return results;
    }

    public static String[] strip(String[] classifications, String... elements) {
    		HashSet<String> tmp = new HashSet<>(Arrays.asList(classifications));
    		tmp.removeAll(Arrays.asList(elements));
    		String[] results = new String[tmp.size()];
    		for(int i=0,j=0;i!=classifications.length;++i) {
    			if(tmp.contains(classifications[i])) {
    				results[j++] = classifications[i];
    			}
    		}
    		return results;
    }
}
