package libiostudy;


import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Log utility.
 * @author jens dietrich
 */
public class Logging {

    static {
        BasicConfigurator.configure();
    }

    static Logger getLogger(String name) {
        return Logger.getLogger(name);
    }
}
