
\begin{table}[!t]
\input{application-stats-jens.tex}
\end{table}

\section{Adaptation of Semantic Versioning (RQ3)}
\label{sec:rq3}
% NOTES:
%
% STRONG: a strong statement is made that semantic versioning should be used
% SOFT: a recommendation is made that semantic versioning should be used
% WEAK: no connection to semantic version but some informal rules
% which are similar are given
% NONE: nothing
% 
% 
% Could talk about caret versioning in Rust / NPM.  What does semantic
% versoining spec stay about this?

The results from Section~\ref{sec:rq1} support the idea that semantic
versioning practices are being applied.  For example, consider the
results for Cargo in Table~\ref{tab:classification:all} which indicate
a large proportion of \textit{var-micro} dependencies.  In other
words, a lot of dependencies are declared as requiring {\em any}
version of a package with the same \verb+major.minor+ number.  This
suggests developers are assuming backwards compatibility between
packages on the \verb+micro+ level at least.  However, we are
interested in whether or not a {\em stronger} connection with semantic
versioning can be established.  And, more specifically, how different
ecosystems adapt the concept of semantic versioning to their needs.

Table~\ref{tab:application:details} provides the results of our
analysis looking at how semantic versioning is adapted in practice.
As discussed in Section \ref{sec:methodology:adaptation}, for each package manager, we
determined whether or not explicit statements were made about using
semantic versioning and what tooling is available.  An explicit
statement is an indication that semantic version should be used in
some way.  We further classified these into: {\em strong}, {\em soft}
and {\em weak} statements, where: a strong statement is a {\em
  requirement} that semantic versioning be used; a {\em soft}
statement is a {\em recommendation} that it should be used; finally, a
{\em weak} statement doesn't explicitly mention semantic versioning,
but gives informal rules which have a similar intent.

{\em Default tool support} reflects the standard tooling that comes with the
package manager, and is classified into: {\em none}, {\em shallow} and
{\em deep}.  Here, shallow support indicates that syntax checking is
performed on dependency versions.  For example, Cargo checks that
versions conform to the format ``\texttt{major.minor.micro}'' and,
likewise, checks dependency patterns (e.g. it accepts
``\texttt{1.2.*}'', but rejects ``\texttt{1.2.*abc}'' and
``\texttt{1.2.**}'').  In contrast, deep support indicates some degree
of {\em static analysis} to provide a stronger assurance that semantic
versioning is used correctly.  For example, in Elm, the command
``\texttt{elm bump}'' attempts to determine the next appropriate
version to use by comparing the current source with the previous
version.  

{\em Other} tool support reflects third-party tooling aimed at
supporting the use of semantic versioning.  Here, {\em shallow} tools
represent the majority of our findings and are only syntactic in
nature (where a single tick indicates $1-10$ tools found, and a double
tick $1-20$).  Indeed, they almost all boil down to two use cases:
libraries for parsing and comparing semantic versions; and, tools for
``bumping'' semantic versions (i.e. where a user asks to increment
{\em major}, {\em minor} or {\em micro} version).  An example of
something which doesn't fit these categories is the NPM semver
calculator\footnote{https://semver.npmjs.com/ , all URLs referenced in
  this Section were accessed on 16 January 19} which, for a given
pattern, identifies which versions match.  In contrast {\em deep}
tools represent those which attempt some form of static analysis to
either generate the next semantic version, or check the given version
is correct.  Since these are more interesting, we examine them in
detail:

\begin{itemize}
\item {\bf (Cargo)}
  \verb+rust-semverver+\footnote{https://github.com/rust-dev-tools/rust-semverver}
  is a proof-of-concept tool for checking {\em backwards compatibility}.
  This tool compares the exposed API
  surface between two versions, reporting any violations.  For
  example, if a public method is removed in the later version, then
  this constitutes a breaking change and requires a different major
  version.
  %Jens: I replaced "prototype tool" (which confused me a bit) by "proof-of-concept tool" 
  % this is also the term used on the website
  % I also removed "This tool is typical of those found " as it is not clear what this refers to

\item {\bf (Maven).}
  Revapi\footnote{\url{https://revapi.org/modules/revapi-maven-plugin}},
  Clirr\footnote{\url{http://clirr.sourceforge.net}}, the Java API
  Compliance Checker
  (JAPICC)\footnote{https://github.com/lvc/japi-compliance-checker}
  and the
  \verb+semantic-versioning+\footnote{https://github.com/jeluard/semantic-versioning}
  library are all tools, like \verb+rust-semverver+, for checking
  backwards compatibility. 

\item {\bf (NPM)} The tool
  \verb+semantic-release+\footnote{https://www.npmjs.com/package/semantic-release}
  provides the main example here, and all others found either extend
  this or are plugins for it.  This tool determines the next suitable
  version using commit messages to determine the extent of changes.
  For example, if one puts ``\verb+BREAKING CHANGE+'' in a commit
  message, then it increments the major version, etc.

\item {\bf (NuGet)} The three tools found here,
  \verb+SemVer.FromAssembly+\footnote{https://www.nuget.org/packages/SemVer.FromAssembly/},
  \verb+SemVerAdvisor+\footnote{https://www.nuget.org/packages/SemVerAdvisor/}
  and
  \verb+SpiseMisu.SemanticVersioning+\footnote{https://www.nuget.org/packages/SpiseMisu.SemanticVersioning/}
  all check backwards compatibility based on exposed API surface.

\item {\bf (Packagist)} Here the only tool identified was
  \verb+semcrement+ which ``{\em compares structure definitions of
    your project's code in between
    commits''}\footnote{https://packagist.org/packages/wick-ed/semcrement}.

\end{itemize}

We must add a precaution regarding the results obtained for
identifying third-party tools.  Whilst our search approach has yielded
a good range of tools and given insight into the relevant communities,
there are certainly tools we are aware of which were missed.  These
include: Japicmp\footnote{\url{http://siom79.github.io/japicmp/}}, JDK
API Diff\footnote{\url{https://github.com/AdoptOpenJDK/jdk-api-diff}},
JDiff\footnote{\url{http://javadiff.sourceforge.net}} and the
\verb+SemanticVersionEnforcer+\footnote{\url{https://www.nuget.org/packages/SemanticVersionEnforcer/}}.
While those tools are often conceptually interesting, the fact that they did not occur in search results 
reflects that they are not widely known and haven't seen significant uptake within the respective communities.

% Jens: I inserted the next paragraph to add missing descriptions for the last thre columns
Finally, the last three columns in Table~\ref{tab:application:details} provide a compact summary of the dependency versioning syntax observed in Section~\ref{sec:rq1}, in particular Table~\ref{tab:classification:aggregated}. Uptake is summarised as follows: $>$50\% -- strong, 10-50\% -- significant, 0-10\% -- low, 0\% -- none.\\

%Jens: added the last sentences to provide some context and explain why this does not matter much.


\noindent\fbox{%
  \parbox{\columnwidth}{%
    \textbf{RQ3 How do projects adapt semantic versioning?} Most
    package managers encourage packages to follow semantic versioning,
    though only a few require it.  The majority of package managers
    perform shallow checking of semantic versions, but very few do
    anything beyond this.  Libraries for parsing and comparing
    semantic versions are widespread, and there is some evidence of
    more complex third-party tooling being developed to support proper
    use of semantic versioning.  
    % Jens: I commented out the last sentence, this is really RQ1 and a bit out of context here
    % Finally, there is generally high uptake of the flexible versioning styles.
  }%
}

% djp: The following tools, whilst relevant, did not come up in my
% searches
%
% Japicmp\footnote{\url{http://siom79.github.io/japicmp/}} is another
% tool to compare two versions of a jar archive. The tool analyses two
% given jar files to determine the differences between them. The tool is
% also able to evaluate changes in class file attributes or annotations,
% which make it suitable for annotation-based APIs such as JAXB.  A more
% specialised tool is the JDK API
% Diff\footnote{\url{https://github.com/AdoptOpenJDK/jdk-api-diff}}, an
% extension to japicmp that compares and spots API changes between any
% two different JDK versions.
% JDiff\footnote{\url{http://javadiff.sourceforge.net}} (a Javadoc
% doclet tool that also compares two APIs and report differences in
% details, include packages, classes, constructors, methods, and fields
% which have been removed, added or changed in any way).

% note: from their website "This library does not use the Java
% Reflection API to compute the differences, as the usage of the
% Reflection API makes it necessary to include all classes the jar
% archive under investigation depends on are available on the
% classpath".
