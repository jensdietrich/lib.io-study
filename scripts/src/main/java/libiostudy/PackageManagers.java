package libiostudy;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Definition of valid package managers.
 * Aquired by imported dependencies from Libraries.io-open-data-1.2.0 into postgresql,
 * and queried with: select platform from dependencies group by platform;
 * @author jens dietrich
 */
public class PackageManagers {

    public static final Set<String> ALL = Stream.of(
        "Atom",
        "CPAN",
        "CRAN",
        "Cargo",
        "Dub",
        "Elm",
        "Haxelib",
        "Hex",
        "Homebrew",
        "Maven",
        "NPM",
        "NuGet",
        "Packagist",
        "Pub",
        "Puppet",
        "Pypi",
        "Rubygems"
    ).collect(Collectors.toSet());
}
