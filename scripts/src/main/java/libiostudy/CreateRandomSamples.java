package libiostudy;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Script to create random samples.
 * @author jens dietrich
 */
public class CreateRandomSamples {

    public static Logger LOGGER = Logging.getLogger("classifier");

    public static final File RESULT_FOLDER = new File("out/");
    public static final String SAMPLE_SIZES = "/samplesizes.properties"; // resource ref for classloader
    public static final int PROGRESS_LOG_INTERVAL = 10_000_000;
    public static final String CSV_SEP = "\t";
    public static final int MIN_SAMPLE_SIZE_PER_CLASSIFICATION = 10;

    public static void main (String[] args) throws Exception {

        File ruleFolder = new File(new File(System.getProperty("user.dir")).getParentFile(), "mapping-rules");
        Preconditions.checkState(ruleFolder.exists());
        List<String> packageManagersWithRules = Stream.of(ruleFolder.list())
                .filter(f -> f.endsWith(".rules"))
                .map(f -> f.substring(0, f.lastIndexOf('.')))
                .sorted()
                .collect(Collectors.toList());

        String pckManager = args.length>0?args[0]:null;

        Map<String, Integer> sampleSizes = loadSampleSizes();
        Properties dbProperties = loadDBSettings();
        Class.forName(dbProperties.getProperty("driver"));
        String url = dbProperties.getProperty("url");

        if (pckManager!=null) {
            Preconditions.checkArgument(PackageManagers.ALL.contains(pckManager), "This is not the name of a package manager: " + pckManager);
            Preconditions.checkArgument(packageManagersWithRules.contains(pckManager), "No rule definitions found for: " + pckManager);
            LOGGER.info("Creating sample data for " + pckManager);
            createSampleData(url, ruleFolder, sampleSizes, pckManager);
        }
        else {
            for (String pckManager2:PackageManagers.ALL) {
                LOGGER.info("Creating sample data for " + pckManager2);
                createSampleData(url, ruleFolder, sampleSizes, pckManager2);
            }
        }


    }
    private static void createSampleData(String dbURL,File ruleFolder,Map<String,Integer> sampleSizes,String pckManager) throws Exception {

        List<Rule> rules = parseRules(ruleFolder, pckManager);

        if (rules==null) {
            LOGGER.warn("No rules found for " + pckManager + " -- skip and proceed");
        }
        else {
            Multimap<String, String> dependenciesByClassification = Multimaps.synchronizedMultimap(HashMultimap.create());
            Map<String,String> classifications = new ConcurrentHashMap<>();
            Set<String> dependencies = new HashSet<>();
            try (Connection connection = DriverManager.getConnection(dbURL)) {
                LOGGER.info("Connected to DB");

                String sql = "select dependencyrequirements from dependencies where platform=\'" + pckManager + "\'";
                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                int count = 0;
                while (rs.next()) {
                    count = count + 1;
                    if (count % PROGRESS_LOG_INTERVAL == 0) {
                        LOGGER.info("\treading record #" + count);
                    }
                    String dependency = rs.getString("dependencyrequirements").trim();
                    dependencies.add(dependency);
                }
            } catch (SQLException x) {
                LOGGER.error("Database error", x);
            }

            // classify concurrently
            dependencies.parallelStream().forEach(
                d -> {
                    String cl = classify(d,rules);
                    dependenciesByClassification.put(cl, d);
                    classifications.put(d,cl);
                }
            );

            // computing samples

            Collection<String> sampleList = RandomUtils.getRandomSubset(dependencies, sampleSizes.get(pckManager));
            List<String> samples = new ArrayList<>();
            Map<String, Integer> sampleCountsByClassification = new HashMap<>();

            samples.add("# randomly generated subset for sampling");
            samples.add("# note that the distribution of classifications may differ as the sampling set is derived from the set of *unique* dependencies");
            samples.add("");
            samples.add("# random samples:");
            samples.add("");
            sampleList.stream().forEach(d -> {
                String classification = classifications.get(d);
                sampleCountsByClassification.compute(classification, (k, v) -> v == null ? 1 : v + 1);
                samples.add(d + CSV_SEP + classification);
            });

            File SAMPLE_CSV = new File(RESULT_FOLDER, "sampledata-" + pckManager + ".csv");

            // create additional samples to ensure that each classification is represented
            for (String classification : Classifications.ALL) {
                int sampleCount = sampleCountsByClassification.computeIfAbsent(classification, k -> 0);
                if (dependenciesByClassification.get(classification).size() > 0 && sampleCount < MIN_SAMPLE_SIZE_PER_CLASSIFICATION) {
                    sampleList = RandomUtils.getRandomSubset(dependenciesByClassification.get(classification), MIN_SAMPLE_SIZE_PER_CLASSIFICATION-sampleCount);
                    samples.add("");
                    samples.add("# additional samples for classification: " + classification);
                    samples.add("");
                    sampleList.stream().forEach(d -> {
                        samples.add(d + CSV_SEP + classification);
                    });
                }
            }

            exportSamples(SAMPLE_CSV, samples);
        }

    }


    private static Map<String,Integer> loadSampleSizes () throws Exception {
        Properties data = new Properties();
        data.load(CreateRandomSamples.class.getResourceAsStream(SAMPLE_SIZES));
        Map<String,Integer> sampleSizes = new HashMap<>();
        for (String pckManager:PackageManagers.ALL) {
            String size = data.getProperty(pckManager);
            assert size != null;
            sampleSizes.put(pckManager,Integer.parseInt(size));
        }
        return sampleSizes;
    }

    private static Properties loadDBSettings() throws Exception  {
        String dbConfigFileName = "db.properties";
        Preconditions.checkState(new File(dbConfigFileName).exists(),"db configuration file not found, create file using thre followiong template:\n" +

                "driver=org.postgresql.Driver\n"+
                "url=jdbc:postgresql://localhost:5432/lib.io\n"+
                "url=jdbc:sqlite:/local/scratch/dependencies.db\n" +

                "sqlite example: \n" +
                "driver=org.sqlite.JDBC\n" +
                "url=jdbc:sqlite:/local/scratch/dependencies.db");
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(dbConfigFileName)) {
            properties.load(reader);
        }
        Preconditions.checkState(properties.containsKey("driver"));
        Preconditions.checkState(properties.containsKey("url"));
        return properties;
    }


    private static String classify(String dependency, List<Rule> rules) {
        for (Rule rule:rules) {
            if (rule.getMatch().matcher(dependency).matches()) {
                return rule.getClassification();
            }
        }
        return Classifications.DEFAULT;
    }

    private static List<Rule> parseRules(File ruleFolder, String packageManager) {
        LOGGER.info("Parsing rules for: " + packageManager);
        try {
            return new RuleParser().parse(new File(ruleFolder, packageManager + ".rules"), true);
        }
        catch (IOException x) {
            LOGGER.fatal("Cannot read rules",x);
            return null;
        }
    }

    private static void exportSamples (File file, List<String> data) throws IOException {
        // print classifications in CSV format
        try (PrintWriter out = new PrintWriter(file)) {
            for (String line:data) {
                out.println(line);
            }
            LOGGER.info("Samples in CSV format exported to " + file.getAbsolutePath());
        }
    }

}
