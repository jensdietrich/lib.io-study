\section{Methodology}
\label{sec:methodology}

In this section, the methodology used to acquire and analyse data is discussed. We will discuss limitations and threats to validity within the subsections as necessary. In the interest of reproducibility, the scripts used to acquire, process and analyse the data are made available in a public repository (\url{https://bitbucket.org/jensdietrich/lib.io-study/}).

%Quality assurance consisted of inspection of sampled data and peer-reviews, this process is also documented in the issue tracker of the respective repository.
% Kelly: this is said below in various sections

\subsection{Dataset Acquisition}
\label{sec:methodology:data}

We used a data set from \texttt{libraries.io}\footnote{\url{https://zenodo.org/record/1196312/files/Libraries.io-open-data-1.2.0.tar.gz} [Accessed: 1 August 2018]} for this study. \texttt{libraries.io} is a service that tracks and indexes package releases from 36 package managers~\footnote{\url{https://libraries.io/about} [Accessed: 1 August 2018]}. The data set contains dependency data in CSV format that we imported into a PostgreSQL database for further analysis and processing. In particular, the dataset contains a \textit{dependencies} table which has versioned dependencies of packages to other packages. This table has 71,884,555 records with  dependency information for packages from the following 17 different package managers: Atom, Cargo, CPAN, CRAN, Dub, Elm, Haxelib, Hex, Homebrew, Maven, NPM, NuGet, Packagist, Pub, Puppet, Pypi and Rubygems.

Atom is a special case as it also allows users to specify dependencies to NPM packages. However, Atom packages are managed in a different repository~\footnote{\url{https://atom.io/packages} [Accessed: 16 Jan 2019]}, and we therefore decided to keep those packages in the data set.

% count records: select count(*) from dependencies;
% count package managers: select platform from dependencies group by platform;

\subsection{Classification Categories}
\label{sec:methodology:classification}

Each package manager uses its own syntax in order to declare dependencies, using a combination of package name and \textit{version constraint}. We were mainly interested in the version constraint part that describes which version(s) of a target package can be used, this is usually a pattern that is applied to versions expressed as \texttt{<major>.<minor>.<micro>}, sometimes followed by additional tokens, such as build numbers.

In order to conduct a comparative study across different package managers, we developed a taxonomy to represent various ways to declare dependencies. The challenge was to strike a suitable compromise between enabling a fine-grained analysis but still making sure that the concepts used were sufficiently abstract to be applied to different package managers. We developed a set of classifications (defined in Table~\ref{tab:classifications}) by iteratively reviewing the dependency declarations across all package managers.

\begin{table}[htb]
	\caption{Dependency version classifications}
	\label{tab:classifications}
	\scriptsize
	\begin{tabular}{|p{1.3cm}p{6.7cm}|}
		\hline
		classification & description \\ \hline

		fixed       		& A dependency on a fixed version (of another package), such as \texttt{1.2.3}. \\

		soft 				& Dependency on a fixed version, such as \texttt{1.2.3}, but the package manager may chose another version in order to resolve dependency constraints, using some notion of closeness or similarity. \\

		var-micro		& Uses a wildcard for the micro part of the version string, such as \texttt{1.2.*}, indicating that the project may depend on any version of a package with a version number starting with \texttt{1.2}. This may include  additional bounds, such as \texttt{1.2.* , < 1.2.4}.\\

		var-minor 		& Uses a wildcard for the minor part of the version string, such as \texttt{1.*}, indicating that the project may depend on any version of a package with a version number starting with \texttt{1}. This may include an additional bound. \\

		any					& Indicating that a project may use any version of the package it depends on, the package manager has unconstrained freedom to decide which one. \\

		at-least			& Indicating a dependency on any version following a specific version (inclusive or exclusive), such as \texttt{[1.2.3,*]}. \\

		at-most			& Indicating a dependency on any version up to a specific version (inclusive or exclusive), such as \texttt{[*,2.0.0]}. \\

		range			& A custom range, such as \texttt{[1.2.0,2.0.0)}, indicating that a project depends on any version from \texttt{1.2.0} to \texttt{2.0.0}. Either range bound can be inclusive or exclusive. \\

		latest				& The dependency should always be resolved to the latest version available, possibly with some qualifier (such as latest-stable, excluding beta versions. \\

		not				& A dependency is declared that explicitly excludes a certain version, usually this is caused by a known issue in this version. \\

		other			& Some custom pattern, for instance, a complex boolean formula combining any of the resolved category.\\

		unresolved	& The dependency string contains unresolved variable references. This case occurs because \texttt{libraries.io} scans project metadata such as (Maven) POMs without understanding their semantics. In a POM, a dependency might be declared by a reference to a variable declared elsewhere, such as \texttt{\${project.version}}. \\

		unclassified & Default value of none of the mapping rules can be applied.  \\
		\hline
	\end{tabular}
\end{table}

\subsection{Classification Rules}
\label{sec:methodology:rules}

For each dependency declaration for all package managers, we mapped the specified version constraints to the classifications in Table~\ref{tab:classifications}. To enable the analysis of the large dataset, this analysis was automated by developing a set of rules that map regular expressions to the appropriate classifications. 

\textbf{Syntactic mapping:} There are two possible approaches: a syntactic mapping solely based on the syntax used, and a semantic mapping taking into account additional interpretations of version constraints, such as how version information is processed. To illustrate this point, consider a version constraint \texttt{>=1.2.0,<1.3.0} (or, using an alternative syntax,  \texttt{$[$1.2.0,1.3.0)}) . Intuitively, this would be classified as \textit{range}. But semantically this corresponds to \textit{var-micro}, and some package managers offer an alternative shortcut syntax for this, such as \texttt{1.2.*}. We decided to use a purely syntactic classification that would classify this constraint as \textit{range} for two reasons. Firstly, it is very hard to accurately implement a semantic mapping, in particular when additional modifiers (such as \textit{beta}, \textit{rc}, ..) are involved that may have special semantics. Secondly, syntax constructs like wild cards are more than just syntactic sugar for the convenience of the programmer, they indicate that this pattern is common and/or supported since developers follow certain rules and conventions that guarantee a certain level of compatibility across versions matching such a pattern. In other words, they are indications of how the community and an individual programmer declaring a dependency have adapted semantic versioning practices. In this sense, \texttt{>=1.2.0,<1.3.0} and  \texttt{1.2.*} are different, and we wanted this difference to be reflected in our analysis.

\textbf{Defining rules:} Rules were developed for each package manager by reviewing its version constraint specification. The rules were defined using a lightweight domain-specific language (DSL) developed for this purpose. Figure \ref{fig:rule} shows a sample classification rule for Maven along with a description of the DSL used. If a version constraint matches the regular expression in the rule, the respective classification is used. The full set of classification rules can be found in the  repository.

\begin{figure}[ht!]
\centering
\includegraphics[width=80mm]{classification_rule.jpg}
\caption{Sample Classification Rule and description of domain-specific language employed \label{fig:rule}}
\end{figure}

For a given rule file, rules are applied top to bottom. If none of the mapping rules can be applied, the default classification \textit{unclassified} was used. This usually indicated that the version string is {invalid} with respect to the package manager specification. Maven is a special case here. The Maven repository and infrastructure is used by different build tools that may use a different syntax to express version constraints. Our classification script (see below) supports the syntax being used by the major package managers (maven, ivy and gradle), but may fail to classify a dependency that uses syntax from a more exotic tool. In this case, \textit{unclassified} is used.

%\begin{lstlisting}[basicstyle=\footnotesize]
%# at least
%test: [1.2.3,)
%test: [1.52,]
%test: [1.52 , ]
%test: (1.2.0,)
%match:(\(|\[)(\w|\.|-|\s)+,\s*(\)|\])
%classify:at-least
%\end{lstlisting}
%Rules have comments (starting with \texttt{\#}), a body consisting of a regular expression, a head containing the classification to be assigned if the version string matches the regular expression in the body, and tests that are evaluated in order to check whether the rules work as intended. 

\textbf{Verifying rules:} We verified the accuracy of the classification using the following process. Firstly, the rule DSL has built-in support for writing tests that check whether certain patterns are classified as expected. These tests are checked before the classification is computed, and the process fails if any of the tests fail. Secondly, rule sets were peer-reviewed. For this purpose, a script was developed that created random samples containing version constraints and their associated classifications (obtained using the regular expressions described above). These were manually peer-reviewed and compared against the specification of the respective package manager. The size of the samples was set to obtain a 95\% confidence level and a 5-10\% confidence interval, additional samples were generated for classifications that were under-represented in the sample to ensure at least 10 records for each classification used for a particular package manager were reviewed. The rules were revised as needed and the peer-review repeated until all version constraints in the sample were correctly classified.
%The discussions following the review process led to changes in several cases, they are documented in the repository issue tracking systems, issues 1-14 and 16-18.

\subsection{Classification Aggregation}
\label{sec:methodology:aggregation}

While the classification scheme provides a fine-grained view on the various patterns used, it is sometimes useful to consider dependency versioning from a more abstract point of view where we are interested to distinguish between the declaration of fixed versions and variable versions of some kind. Since one of the goals of this study is to investigate the uptake of semantic versioning, we also consider syntax that directly supports semantic versioning practices.

The aggregation of classification categories is defined by the following set of rules, using a simple rule syntax:

\begin{lstlisting}[basicstyle=\footnotesize]
SEMVER := var-micro | var-minor
FLEXIBLE := range | soft | any | latest |  not |
           at-least | at-most
FIXED := fixed
OTHER := other | unresolved | unclassified
\end{lstlisting}

The semantics of the rules is straight forward: if a dependency is classified using any category in the body (right side) of the rule, then it is classified in the category in the head (left side) of the rule.

\subsection{Version Ordering}
\label{sec:methodology:ordering}

In order to answer RQ2, it was necessary to identify the first and the last version of each project in the dataset. Using a naive lexicographical order of version strings is not sufficient to achieve this with a sufficient level of accuracy, for instance, while this would yield \texttt{1.2.3 < 1.2.4} as expected, this method would also result in \texttt{1.2.10 < 1.2.9}. We therefore opted for an more accurate approach to first sanitise version strings (removing leading ``\texttt{r}'' or ``\texttt{R}'' preceding version strings), then to tokenise leading substrings matching \verb|\d+(\.\d+)*| and comparing versions by comparing those numerical tokens from left to right. This was then implemented in a script that produced a table consisting of project name, first version, and last version for each package manager.  Those tables were then sampled and peer-reviewed %(issue 21) 
in order to ensure a sufficient level of accuracy.

We did not consider the semantics of additional strings following the numeric parts of the version, (such as ``\texttt{-alpha}'', ``\texttt{-beta}'', or ``\texttt{rc1}''), and used the lexicographical order for those suffixes. The reason for this decision was that there is a large number of custom prefixes (including hashes referring to commits), that are often platform-- and project-specific.  This can lead to cases where our method may not be able to extract the very first or the very last version in the dataset. For instance, we infer \texttt{1.2.3-ga < 1.2.3-rc} which is incorrect if one takes the meaning of the respective suffixes (\texttt{-ga} -- general availability, \texttt{-rc} -- release candidate) into account. However, we are confident that the first version our approach extracts always precedes the last version.

\subsection{Adaptation of Semantic Versioning}
\label{sec:methodology:adaptation}

% To address RQ3, we canvassed specifications and other documentation for the relevant package managers looking for explicit statements about semantic versioning and its application; likewise, we explored the availability of tools for checking or enforcing semantic versioning. We then conducted a web-search to look for other relevant semantic versioning tools that are specifically not associated with specific package managers. In doing so, we searched online using the following Google search query: \texttt{"semver calculator" OR "semver tool" OR "semantic versioning calculator" OR "semantic versioning tool"}\footnote{https://goo.gl/rRkRE2 (search results were retrieved on 20 Nov 2018)}. We then manually checked all the results from this query\footnote{the search returns 891 results, as in 19 November 2018} to identify any semantic versioning tools that have been implemented. In order to minimise the risk of missing key tools, we followed the general search process with a targeted search by going through the search facility of each package manager to identify relevant semantic versioning tools. We search using rather more general terms (i.e., semver  "semantic versioning") and then manually checked all the retrieved results. We avoided using OR operator because that not all search tools supported logical disjunctions. In cases where no results was returned, we searched using the two terms above individually. This was done for all 17 package managers.
To address RQ3, we needed to identify the stance package managers take towards semantic versioning 
and the tools used to support semantic versioning.
To do this, we canvassed specifications and other documentation
for the relevant package managers looking for explicit statements
about semantic versioning and its application; likewise, we exercised
the tools themselves to check what level of support they provided and,
finally, we explored the availability of third-party tools and
libraries to support semantic versioning.  To do the latter, we
conducted two searches for each package manager.  First, we searched
online using the Google search query: ``\texttt{PKGMGR (semver OR
  "semantic versioning")}'', where ``\texttt{PKGMGR}'' was replaced
with the name of the package manager in question (e.g. Cargo);
secondly, we located the associated online package repository and used
its search tool with the query ``\texttt{semver "semantic
  versioning"}''.  In both cases, we restricted ourselves to exploring
the first twenty results reported~\footnote{The searches were
  respectively conducted on 17th and 18th Jan, 2019.}.  We also considered any tool that was mentioned in the responses to survey question 4 (see Section \ref{sec:methodology:survey}).

\input{survey}
