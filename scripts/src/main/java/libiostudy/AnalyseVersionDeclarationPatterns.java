package libiostudy;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import libiostudy.util.SplitWideLatexTable;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Count the  how packages declare their versions.
 * @author jens dietrich
 */
public class AnalyseVersionDeclarationPatterns {

    public static Logger LOGGER = Logging.getLogger("count-dependency-kind");


    // patterns, must be used in correct order as some strings match MMM and MMX
    public static final Pattern M = Pattern.compile("\\d+");
    public static final Pattern MM = Pattern.compile("\\d+\\.\\d+");
    public static final Pattern MMX = Pattern.compile("\\d+\\.\\d+(.)*");
    public static final Pattern MMM = Pattern.compile("\\d+\\.\\d+\\.\\d+");
    public static final Pattern MMMX = Pattern.compile("\\d+\\.\\d+\\.\\d+(.)*");


    public static final Pattern vMX = Pattern.compile("(v|V)\\d+\\.(.)*");

    public static final File RESULT_FOLDER = new File("out/");
    public static final File UNCLASSIFIED_VERSIONS_FOLDER = new File("out/");
    public static final File DATA_CSV = new File(RESULT_FOLDER,"versions.csv");
    public static final int PROGRESS_LOG_INTERVAL = 1_000_000;
    public static final String CSV_SEP = "\t";
    public static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#,###,###");
    public static final int QUEUE_SIZE = 5_000_000;
    public static final int CONSUMER_THREADS = Runtime.getRuntime().availableProcessors() - 1;
    public static final int UNCLASSIFIED_THRESHOLD = 1_000;

    public static void main (String[] args) throws Exception {

        long t1 = System.currentTimeMillis();


        String tmp = null;
        if (args.length==1) {
            tmp = args[0];
            Preconditions.checkArgument(PackageManagers.ALL.contains(tmp),"This is not the name of a package manager: " + tmp);
            LOGGER.info("Restricting classification to package manager " + tmp);
        }
        final String pckManager = tmp;

        Properties dbProperties = DBSettings.loadDBSettings();
        Class.forName(dbProperties.getProperty("driver"));
        String url = dbProperties.getProperty("url");

        Map<String,Integer> mCountsByPackageManager = new ConcurrentHashMap<>();
        Map<String,Integer> mmCountsByPackageManager = new ConcurrentHashMap<>();
        Map<String,Integer> mmxCountsByPackageManager = new ConcurrentHashMap<>();
        Map<String,Integer> mmmCountsByPackageManager = new ConcurrentHashMap<>();
        Map<String,Integer> mmmxCountsByPackageManager = new ConcurrentHashMap<>();
        Map<String,Integer> otherCountsByPackageManager = new ConcurrentHashMap<>();
        Map<String,Integer> countsByPackageManager = new ConcurrentHashMap<>();

        Map<String,Collection<String>> otherSamplesByPackageManagers = new ConcurrentHashMap<>();
        Map<String,Collection<String>> mmxSamplesByPackageManagers = new ConcurrentHashMap<>();
        Map<String,Collection<String>> mmmxSamplesByPackageManagers = new ConcurrentHashMap<>();

        BlockingQueue<String[]> queue = new ArrayBlockingQueue<String[]>(QUEUE_SIZE);
        AtomicBoolean QUERY_DONE = new AtomicBoolean(false);

        Runnable producer = () -> {
            try (Connection connection = DriverManager.getConnection(url)) {
                LOGGER.info("Connected to DB");

                String sql = "select platform, versionnumber from dependencies";
                if (pckManager != null) {
                    sql = sql + " where platform=\'" + pckManager + "\'";
                }

                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                LOGGER.info("Received result set");
                int count = 0;
                while (rs.next()) {
                    count = count + 1;
                    if (count % PROGRESS_LOG_INTERVAL == 0) {
                        LOGGER.info("\treading record #" + count + " , queue size: " + queue.size());
                    }
                    String packageManager = rs.getString("platform").trim();
                    String versionnumber = rs.getString("versionnumber").trim();
                    try {
                        queue.put(new String[]{packageManager,versionnumber});
                    }
                    catch (InterruptedException x) {
                        LOGGER.error("Error putting element in queue",x);
                    }
                }
                QUERY_DONE.set(true);
                LOGGER.info("Done reading data");
            }
            catch (SQLException x) {
                QUERY_DONE.set(true);
                LOGGER.error("Database error",x);
            }
        };

        AtomicInteger consumerCounter = new AtomicInteger();

        class Consumer implements Runnable  {
            public void run() {
                while (!(QUERY_DONE.get() && queue.isEmpty())) {
                    String[] record = queue.poll();
                    if (record != null) { // null if queue is empty
                        int step = consumerCounter.incrementAndGet();
                        if (step % PROGRESS_LOG_INTERVAL == 0) {
                            LOGGER.info("\tclassifying record #" + step + " , queue size: " + queue.size());
                        }
                        String packageManager = record[0];
                        String version = record[1];

                        version = sanitise(version,packageManager);

                        if (M.matcher(version).matches()) {
                            mCountsByPackageManager.compute(packageManager,(k,v) -> v==null?1:v+1);
                        }
                        else if (MM.matcher(version).matches()) {
                            mmCountsByPackageManager.compute(packageManager,(k,v) -> v==null?1:v+1);
                        }
                        else if (MMM.matcher(version).matches()) {
                            mmmCountsByPackageManager.compute(packageManager,(k,v) -> v==null?1:v+1);
                        }
                        else if (MMMX.matcher(version).matches()) {
                            mmmxCountsByPackageManager.compute(packageManager,(k,v) -> v==null?1:v+1);
                            addSample(mmmxSamplesByPackageManagers,packageManager,version);
                        }
                        else if (MMX.matcher(version).matches()) {
                            mmxCountsByPackageManager.compute(packageManager,(k,v) -> v==null?1:v+1);
                            addSample(mmxSamplesByPackageManagers,packageManager,version);
                        }
                        else {
                            otherCountsByPackageManager.compute(packageManager,(k,v) -> v==null?1:v+1);
                            addSample(otherSamplesByPackageManagers,packageManager,version);
                        }
                        countsByPackageManager.compute(packageManager,(k,v) -> v==null?1:v+1);

                    }
                }
            }


        };

        ExecutorService executor = Executors.newCachedThreadPool();
        executor.submit(producer);
        Stream.generate(() -> new Consumer()).limit(CONSUMER_THREADS).forEach(c -> executor.submit(c));
        executor.shutdown();
        executor.awaitTermination(1,TimeUnit.DAYS);



        LOGGER.info("Aggregating and exporting classifications");

        Set<String> packageManagers = pckManager==null ? PackageManagers.ALL : Sets.newHashSet(pckManager);

        // print classifications in CSV format
        try (PrintWriter out = new PrintWriter(new FileWriter(DATA_CSV))) {
            String firstLine = "package manager " + CSV_SEP + "MMM" + CSV_SEP + "MMMX" + CSV_SEP + "MM" + CSV_SEP + "MMX" + CSV_SEP + "M" + CSV_SEP + "others" + CSV_SEP + "all";
            out.println(firstLine);
            for (String packageManager:packageManagers) {
                out.print(packageManager);
                addIntCell(out,packageManager,mmmCountsByPackageManager);
                addIntCell(out,packageManager,mmmxCountsByPackageManager);
                addIntCell(out,packageManager,mmCountsByPackageManager);
                addIntCell(out,packageManager,mmxCountsByPackageManager);
                addIntCell(out,packageManager,mCountsByPackageManager);
                addIntCell(out,packageManager,otherCountsByPackageManager);
                addIntCell(out,packageManager,countsByPackageManager);
                out.println();
            }
            LOGGER.info("Results in CSV format exported to " + DATA_CSV.getAbsolutePath());
        }

        long t2 = System.currentTimeMillis();
        LOGGER.info("Done, this took " + (t2-t1) + " ms");

        LOGGER.info("Exporting sample file(s)");


        for (String packageManager:packageManagers) {
            exportSamples("other",packageManager,otherSamplesByPackageManagers);
            exportSamples("mmx",packageManager,mmxSamplesByPackageManagers);
            exportSamples("mmmx",packageManager,mmmxSamplesByPackageManagers);
        }


    }

    private static void exportSamples(String kind, String packageManager, Map<String, Collection<String>> map) throws IOException {
        File file = new File(UNCLASSIFIED_VERSIONS_FOLDER,"samples-versions-" + packageManager+"-"+kind+".txt");
        Collection<String> samples = map.get(packageManager);
        if (samples!=null && samples.size()>0) {
            List<String> lines = samples.stream().collect(Collectors.toList());
            FileUtils.writeLines(file,lines);
        }
    }

    private static void addSample(Map<String, Collection<String>> map, String packageManager,String version) {
        Collection<String> samples = map.computeIfAbsent(packageManager,p -> Collections.synchronizedSet(new HashSet<>()));
        if (samples.size()<UNCLASSIFIED_THRESHOLD) {
            samples.add(version);
        }
    }


    // heuristics how to sanitise version strings
    public static String sanitise(String version, String packageManager) {
        if (vMX.matcher(version).matches()) {
            return version.substring(1);
        }

        return version;
    }


    private static void addIntCell(PrintWriter out, String packageManager, Map<String, Integer> values) {
        out.print(CSV_SEP);
        Integer value = values.get(packageManager);
        if (value==null) value = 0;
        String sValue = NUMBER_FORMAT.format(value);
        out.print(sValue);
    }


}
