package libiostudy.util;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility to split a wide latex table.
 * @author jens dietrich
 */
public class SplitWideLatexTable {

    private static Logger LOGGER = Logger.getLogger("splitlatex");

    public static void main(String[] args) throws Exception {

        BasicConfigurator.configure();
        split(
                new File("out/classification.tex"),
                new File("out/classification-part1.tex"),
                new File("out/classification-part2.tex"),
                1,7,"{|l|rrrrrrr|}","{|l|rrrrrrr|}");
    }

    public static void split(File latex,File part1,File part2,int keepCols,int splitAt,String tableSpec1,String tableSpec2) throws Exception {
        Preconditions.checkNotNull(latex);
        Preconditions.checkArgument(latex.exists());
        Preconditions.checkArgument(!latex.isDirectory());
        Preconditions.checkArgument(latex.getName().endsWith(".tex"));

        LOGGER.info("Splitting " + latex.getAbsolutePath());

        List<String> linesPart1 = new ArrayList<>();
        List<String> linesPart2 = new ArrayList<>();
        linesPart1.add("\\begin{tabular}" + tableSpec1);
        linesPart2.add("\\begin{tabular}" + tableSpec2);

        List<String> input = Files.readLines(latex, Charset.defaultCharset());



        for (String line:input) {
            String table1Row = "";
            String table2Row = "";
            line = line.trim();

            if (line.startsWith("\\begin{tabular}")) {
                //ignore
            } else if (line.startsWith("\\end{tabular}")) {
                // ignore
            }
            else if (line.equals("\\hline")) {
                // ignore
            }
            else if (line.startsWith("%")) {
                // ignore
            }
            else {
                String[] cells = line.split("&");
                for (int i=0;i<cells.length;i++) {
                    if (i<keepCols) {
                        if (i>0) {
                            table1Row = table1Row + " & ";
                            table2Row = table2Row + " & ";
                        }
                        table1Row = table1Row + cells[i];
                        table2Row = table2Row + cells[i];
                    }
                    else if (i<=splitAt) {
                        if (i>0) {
                            table1Row = table1Row + " & ";
                        }
                        table1Row = table1Row + cells[i];
                    }
                    else {
                        if (i>0) {
                            table2Row = table2Row + " & ";
                        }
                        table2Row = table2Row + cells[i];
                    }
                }
                table1Row = table1Row + "\\\\";
                // table2Row already ands with \\
            }
            if (!table1Row.isEmpty()) linesPart1.add(table1Row);
            if (!table2Row.isEmpty()) linesPart2.add(table2Row);
        }
        addHLines(linesPart1);
        addHLines(linesPart2);
        linesPart1.add("\\end{tabular}");
        linesPart2.add("\\end{tabular}");


        // write
        FileUtils.writeLines(part1,linesPart1);
        LOGGER.info("Part 1 written to " + part1.getAbsolutePath());

        FileUtils.writeLines(part2,linesPart2);
        LOGGER.info("Part 2 written to " + part2.getAbsolutePath());
    }

    private static void addHLines(List<String> rows) {
        rows.add(2,"\\hline");
        rows.add(1,"\\hline");
        rows.add("\\hline");
    }
}
