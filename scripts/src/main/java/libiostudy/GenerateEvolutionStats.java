package libiostudy;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.collect.Sets;
import com.google.common.math.Stats;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

;

/**
 * Compute stats how dependencies change between first and last version.
 * Queries need an index for this script to be sufficiently fast, the index can be generated using:
 * create index PROJECTVERSION on dependencies(platform,projectname,versionnumber);
 * @author jens dietrich
 */
public class GenerateEvolutionStats {

    public static Logger LOGGER = Logging.getLogger("compute-evolution-stats");
    public static File FIRST_LAST_DATA_FOLDER = new File("out/");
    public static final File RESULT_FOLDER = new File("out/");
    public static final File STATS_CSV = new File(RESULT_FOLDER,"evolution-stats.csv");
    public static final File STATS_LATEX1 = new File(RESULT_FOLDER,"evolution-stats1.tex");
    public static final File STATS_LATEX2 = new File(RESULT_FOLDER,"evolution-stats2.tex");
    public static final String CSV_SEP = "\t";
    public static final DecimalFormat DOUBLE_FORMAT = new DecimalFormat("###.##");
    public static final NumberFormat INT_FORMAT = new DecimalFormat("#,###,###");


    public static enum StatsKeys {
        PROJECTS,
        PROJECTS_WITH_ONE_VERSION,
        AVG_VERSION_COUNT,
        MAX_VERSION_COUNT,
        STDDEV_VERSION_COUNT,
        AVG_FIRST_VERSION_DEPENDENCY_COUNT,
        MAX_FIRST_VERSION_DEPENDENCY_COUNT,
        STDDEV_FIRST_VERSION_DEPENDENCY_COUNT,
        AVG_LAST_VERSION_DEPENDENCY_COUNT,
        MAX_LAST_VERSION_DEPENDENCY_COUNT,
        STDDEV_LAST_VERSION_DEPENDENCY_COUNT,
        AVG_SEMVER_IN_FIRST_VERSION,
        STDDEV_SEMVER_IN_FIRST_VERSION,
        AVG_SEMVER_IN_LAST_VERSION,
        STDDEV_SEMVER_IN_LAST_VERSION,
        AVG_FLEXI_IN_FIRST_VERSION,
        STDDEV_FLEXI_IN_FIRST_VERSION,
        AVG_FLEXI_IN_LAST_VERSION,
        STDDEV_FLEXI_IN_LAST_VERSION,
        PROJECT_USING_SEMVER_V1,
        PROJECT_USING_FLEXI_V1,
        PROJECT_ADAPTING_SEMVER,
        PROJECT_DROPPING_SEMVER,
        PROJECT_ADAPTING_FLEXI,
        PROJECT_DROPPING_FLEXI
    }

    public static void main (String[] args) throws Exception {

        long t1 = System.currentTimeMillis();

        String tmp = null;
        if (args.length==1) {
            tmp = args[0];
            Preconditions.checkArgument(PackageManagers.ALL.contains(tmp),"This is not the name of a package manager: " + tmp);
            LOGGER.info("Restricting classification to package manager " + tmp);
        }
        final String pckManager = tmp;
        Set<String> packageManagers = pckManager==null ? PackageManagers.ALL : Sets.newHashSet(pckManager);

        // read rules
        File ruleFolder = new File(new File(System.getProperty("user.dir")).getParentFile(),"mapping-rules");
        Preconditions.checkState(ruleFolder.exists());


        // prep database query
        Properties dbProperties = DBSettings.loadDBSettings();
        Class.forName(dbProperties.getProperty("driver"));
        String url = dbProperties.getProperty("url");

        Map<String,Map<StatsKeys,Object>> statsByPackageManager = new HashMap<>();

        try (Connection connection = DriverManager.getConnection(url)) {
            LOGGER.info("Connected to DB");
            AtomicInteger count = new AtomicInteger(0);
            for (String packageManager:packageManagers) {

                LOGGER.info("Processing " + packageManager);
                List<Rule> classificationRules = RunClassification.parseRules(ruleFolder, packageManager);
                Map<String,String> aggregationRules = RunClassification.parseAggregationRules(ruleFolder);

                // load first-last file
                Map<String,String[]> firstAndLastByProject = loadFirstAndLast(packageManager);
                PreparedStatement stmnt = connection.prepareStatement("select dependencyrequirements from dependencies where platform=? and projectname=? and versionnumber=?");

                Map<StatsKeys,Object> stats = new HashMap<>();
                statsByPackageManager.put(packageManager,stats);

                int projectsWithOnlyOneVersion = 0;
                int projectsUsingSemVerInFirstVersion = 0;
                int projectsAdaptingSemVer = 0;
                int projectsDroppingSemVer = 0;
                int projectsUsingFlexiInFirstVersion = 0;
                int projectsAdaptingFlexi = 0;
                int projectsDroppingFlexi = 0;



                List<Integer> versionCounts = new ArrayList<>();
                List<Integer> dependenciesInFirstVersionCounts = new ArrayList<>();
                List<Integer> dependenciesInLastVersionCounts = new ArrayList<>();
                List<Double> semverShareInFirstVersion = new ArrayList<>();
                List<Double> semverShareInLastVersion = new ArrayList<>();
                List<Double> flexibleShareInFirstVersion = new ArrayList<>();
                List<Double> flexibleShareInLastVersion = new ArrayList<>();

                // samples to collect
                List<String> projectNamesAdpatingSemVer = new ArrayList<>();
                List<String> projectNamesDroppingSemVer = new ArrayList<>();
                List<String> projectNamesAdpatingFlexi = new ArrayList<>();
                List<String> projectNamesDroppingFlexi = new ArrayList<>();

                for (String project:firstAndLastByProject.keySet()) {

                    if (log(count.incrementAndGet())) {
                        LOGGER.info("Processing project " + count.get());
                    }

                    String[] evolution = firstAndLastByProject.get(project);
                    String firstVersion = evolution[0];
                    String lastVersion = evolution[1];
                    int versionCount = Integer.parseInt(evolution[2]);
                    versionCounts.add(versionCount);

                    if (versionCount==1) {
                        assert firstVersion.equals(lastVersion);
                        projectsWithOnlyOneVersion = projectsWithOnlyOneVersion+1;
                    }

                    else {

                        // first version
                        int dependenciesInFirstVersion = 0;
                        Map<String,Integer> aggregatedDependencyClassificationCountsInFirstVersion = new HashMap<>();
                        stmnt.setString(1, packageManager);
                        stmnt.setString(2, project);
                        stmnt.setString(3, firstVersion);
                        ResultSet rs = stmnt.executeQuery();
                        while (rs.next()) {
                            dependenciesInFirstVersion = dependenciesInFirstVersion + 1;
                            String dependencyRequirement = rs.getString("dependencyrequirements");
                            String classification = RunClassification.doClassify(dependencyRequirement,classificationRules);
                            String category = aggregationRules.get(classification);
                            assert category!=null;
                            aggregatedDependencyClassificationCountsInFirstVersion.compute(category,(k,v) -> v==null?1:v+1);

                        }
                        dependenciesInFirstVersionCounts.add(dependenciesInFirstVersion);
                        rs.close();
                        int semverCount =  aggregatedDependencyClassificationCountsInFirstVersion.computeIfAbsent("SEMVER",k -> 0);
                        int flexibleCount =  aggregatedDependencyClassificationCountsInFirstVersion.computeIfAbsent("FLEXIBLE",k -> 0);
                        int fixedCount =  aggregatedDependencyClassificationCountsInFirstVersion.computeIfAbsent("FIXED",k -> 0);
                        int otherCount = aggregatedDependencyClassificationCountsInFirstVersion.computeIfAbsent("OTHER",k -> 0);
                        if (semverCount + flexibleCount + fixedCount > 0) {
                            double semverShare = ((double) semverCount) / ((double) (semverCount + flexibleCount + fixedCount));
                            double flexiShare = ((double) (semverCount + flexibleCount)) / ((double) (semverCount + flexibleCount + fixedCount));
                            assert !Double.isNaN(semverShare);
                            assert !Double.isNaN(flexiShare);
                            semverShareInFirstVersion.add(semverShare);
                            flexibleShareInFirstVersion.add(flexiShare);
                        }
                        boolean usesFlexInFirstVersion = (semverCount + flexibleCount) > 0;
                        boolean usesSemverInFirstVersion = semverCount > 0;
                        boolean usesOtherInFirstVersion = otherCount > 0;

                        // last version
                        int dependenciesInLastVersion = 0;
                        Map<String,Integer> aggregatedDependencyClassificationCountsInLastVersion = new HashMap<>();
                        stmnt.setString(3, lastVersion);
                        rs = stmnt.executeQuery();
                        while (rs.next()) {
                            dependenciesInLastVersion = dependenciesInLastVersion + 1;
                            String dependencyRequirement = rs.getString("dependencyrequirements");
                            String classification = RunClassification.doClassify(dependencyRequirement,classificationRules);
                            String category = aggregationRules.get(classification);
                            assert category!=null;
                            aggregatedDependencyClassificationCountsInLastVersion.compute(category,(k,v) -> v==null?1:v+1);
                        }
                        dependenciesInLastVersionCounts.add(dependenciesInLastVersion);
                        rs.close();
                        semverCount =  aggregatedDependencyClassificationCountsInLastVersion.computeIfAbsent("SEMVER",k -> 0);
                        flexibleCount =  aggregatedDependencyClassificationCountsInLastVersion.computeIfAbsent("FLEXIBLE",k -> 0);
                        fixedCount =  aggregatedDependencyClassificationCountsInLastVersion.computeIfAbsent("FIXED",k -> 0);
                        otherCount =  aggregatedDependencyClassificationCountsInLastVersion.computeIfAbsent("OTHER",k -> 0);
                        if (semverCount + flexibleCount + fixedCount > 0) {
                            double semverShare = ((double) semverCount) / ((double) (semverCount + flexibleCount + fixedCount));
                            double flexiShare = ((double) (semverCount + flexibleCount)) / ((double) (semverCount + flexibleCount + fixedCount));
                            assert !Double.isNaN(semverShare);
                            assert !Double.isNaN(flexiShare);
                            semverShareInLastVersion.add(semverShare);
                            flexibleShareInLastVersion.add(flexiShare);
                        }
                        boolean usesFlexInLastVersion = (semverCount + flexibleCount) > 0;
                        boolean usesSemverInLastVersion = semverCount > 0;
                        boolean usesOtherInLastVersion = otherCount > 0;

                        boolean usesFlexInFirstVersion2 = usesSemverInFirstVersion || usesFlexInFirstVersion;
                        boolean usesFlexInLastVersion2 = usesSemverInLastVersion || usesFlexInLastVersion;
                        if (usesSemverInFirstVersion) projectsUsingSemVerInFirstVersion = projectsUsingSemVerInFirstVersion + 1;
                        if (usesFlexInFirstVersion2) projectsUsingFlexiInFirstVersion = projectsUsingFlexiInFirstVersion + 1;

                        if (!usesOtherInFirstVersion && !usesOtherInLastVersion) {
                            if (!usesSemverInFirstVersion && usesSemverInLastVersion) {
                                projectsAdaptingSemVer = projectsAdaptingSemVer + 1;
                                projectNamesAdpatingSemVer.add(project);
                            }
                            if (!usesFlexInFirstVersion2 && usesFlexInLastVersion2) {
                                projectsAdaptingFlexi = projectsAdaptingFlexi + 1;
                                projectNamesAdpatingFlexi.add(project);
                            }
                            if (usesSemverInFirstVersion && !usesSemverInLastVersion) {
                                projectsDroppingSemVer = projectsDroppingSemVer + 1;
                                projectNamesDroppingSemVer.add(project);
                            }
                            if (usesFlexInFirstVersion2 && !usesFlexInLastVersion2) {
                                projectsDroppingFlexi = projectsDroppingFlexi + 1;
                                projectNamesDroppingFlexi.add(project);
                            }
                        }
                    }
                }

                // do some stats
                Stats versionCountStats = Stats.of(versionCounts);
                Stats dependenciesInFirstVersionStats = Stats.of(dependenciesInFirstVersionCounts);
                Stats dependenciesInLastVersionStats = Stats.of(dependenciesInLastVersionCounts);

                Stats flexiFirstStats = Stats.of(flexibleShareInFirstVersion);
                Stats flexiLastStats = Stats.of(flexibleShareInLastVersion);
                Stats semverFirstStats = Stats.of(semverShareInFirstVersion);
                Stats semverLastStats = Stats.of(semverShareInLastVersion);

                stats.put(StatsKeys.PROJECTS,firstAndLastByProject.size());
                stats.put(StatsKeys.PROJECTS_WITH_ONE_VERSION,projectsWithOnlyOneVersion);

                stats.put(StatsKeys.AVG_VERSION_COUNT,versionCountStats.mean());
                stats.put(StatsKeys.MAX_VERSION_COUNT,versionCountStats.max());
                stats.put(StatsKeys.STDDEV_VERSION_COUNT,versionCountStats.populationStandardDeviation());

                stats.put(StatsKeys.AVG_FIRST_VERSION_DEPENDENCY_COUNT,dependenciesInFirstVersionStats.mean());
                stats.put(StatsKeys.MAX_FIRST_VERSION_DEPENDENCY_COUNT,dependenciesInFirstVersionStats.max());
                stats.put(StatsKeys.STDDEV_FIRST_VERSION_DEPENDENCY_COUNT,dependenciesInFirstVersionStats.populationStandardDeviation());

                stats.put(StatsKeys.AVG_LAST_VERSION_DEPENDENCY_COUNT,dependenciesInLastVersionStats.mean());
                stats.put(StatsKeys.MAX_LAST_VERSION_DEPENDENCY_COUNT,dependenciesInLastVersionStats.max());
                stats.put(StatsKeys.STDDEV_LAST_VERSION_DEPENDENCY_COUNT,dependenciesInLastVersionStats.populationStandardDeviation());

                stats.put(StatsKeys.AVG_SEMVER_IN_FIRST_VERSION,semverFirstStats.mean());
                stats.put(StatsKeys.STDDEV_SEMVER_IN_FIRST_VERSION,semverFirstStats.populationStandardDeviation());

                stats.put(StatsKeys.AVG_SEMVER_IN_LAST_VERSION,semverLastStats.mean());
                stats.put(StatsKeys.STDDEV_SEMVER_IN_LAST_VERSION,semverLastStats.populationStandardDeviation());

                stats.put(StatsKeys.AVG_FLEXI_IN_FIRST_VERSION,flexiFirstStats.mean());
                stats.put(StatsKeys.STDDEV_FLEXI_IN_FIRST_VERSION,flexiFirstStats.populationStandardDeviation());

                stats.put(StatsKeys.AVG_FLEXI_IN_LAST_VERSION,flexiLastStats.mean());
                stats.put(StatsKeys.STDDEV_FLEXI_IN_LAST_VERSION,flexiLastStats.populationStandardDeviation());

                stats.put(StatsKeys.PROJECT_USING_SEMVER_V1,projectsUsingSemVerInFirstVersion);
                stats.put(StatsKeys.PROJECT_USING_FLEXI_V1,projectsUsingFlexiInFirstVersion);
                stats.put(StatsKeys.PROJECT_ADAPTING_SEMVER,projectsAdaptingSemVer);
                stats.put(StatsKeys.PROJECT_DROPPING_SEMVER,projectsDroppingSemVer);
                stats.put(StatsKeys.PROJECT_ADAPTING_FLEXI,projectsAdaptingFlexi);
                stats.put(StatsKeys.PROJECT_DROPPING_FLEXI,projectsDroppingFlexi);

                LOGGER.info("Done processing data for " + packageManager);

                // export samples
                exportProjectList(projectNamesAdpatingFlexi,new File(RESULT_FOLDER,"projects-adapting-flexi-"+packageManager+".txt"));
                exportProjectList(projectNamesDroppingFlexi,new File(RESULT_FOLDER,"projects-dropping-flexi-"+packageManager+".txt"));
                exportProjectList(projectNamesAdpatingSemVer,new File(RESULT_FOLDER,"projects-adapting-semver-"+packageManager+".txt"));
                exportProjectList(projectNamesDroppingSemVer,new File(RESULT_FOLDER,"projects-dropping-semver-"+packageManager+".txt"));
            }
            LOGGER.info("Done reading data");
        }
        catch (SQLException x) {
            LOGGER.error("Database error",x);
        }
        long t2 = System.currentTimeMillis();
        LOGGER.info("Done, this took " + (t2-t1) + " ms");


        LOGGER.info("Exporting stats");
        printAndExportResultsAsCSV(statsByPackageManager,STATS_CSV);
        printAndExportResultsAsLatex(statsByPackageManager,STATS_LATEX1,
            new String[]{"","PROJ","ONE","AVG","STDEV","AVG1","STDEV1","AVGL","STDEVL"},
            "|l|rr|rr|rr|rr|",
            "tab:evolution:1",
            "Project evolution by package manager (PROJ - project count, ONE - projects with only one version, AVG/STDEV - average / standard deviation of number of versions per project, " +
                "AVG1 / STDEV1 - average / standard deviation of number of dependencies in first version, AVGL / STDL - average / standard deviation of number of dependencies in last version",
            new StatsKeys[]{StatsKeys.PROJECTS,StatsKeys.PROJECTS_WITH_ONE_VERSION,StatsKeys.AVG_VERSION_COUNT,StatsKeys.STDDEV_VERSION_COUNT,StatsKeys.AVG_FIRST_VERSION_DEPENDENCY_COUNT,StatsKeys.STDDEV_FIRST_VERSION_DEPENDENCY_COUNT,StatsKeys.AVG_LAST_VERSION_DEPENDENCY_COUNT,StatsKeys.STDDEV_LAST_VERSION_DEPENDENCY_COUNT}
        );
        printAndExportResultsAsLatex(statsByPackageManager,STATS_LATEX2,
            new String[]{"","projects","SEMV1","SEMV+","SEMV-","FLEX1","FLEX+","FLEX-"},
            "|l|r|rrr|rrr|",
            "tab:evolution:2",
            "Adaptation of flexible dependencies by package manager (PROJ - project count, SEMV1 / FLEX1 - projects using semantic versioning / flexible dependency syntax in first version, " +
                    "SEM+ / SEM- - projects adapting / dropping semantic dependency versioning syntax between first and last version, FLEX+ / FLEX- - projects adapting / dropping flexible dependency syntax between first and last version",

                new StatsKeys[]{StatsKeys.PROJECTS,StatsKeys.PROJECT_USING_SEMVER_V1,StatsKeys.PROJECT_ADAPTING_SEMVER,StatsKeys.PROJECT_DROPPING_SEMVER,StatsKeys.PROJECT_USING_FLEXI_V1,StatsKeys.PROJECT_ADAPTING_FLEXI,StatsKeys.PROJECT_DROPPING_FLEXI}
        );

    }

    private static void exportProjectList(List<String> projects, File file) throws IOException {
        if (!projects.isEmpty()) {
            Collections.sort(projects);
            FileUtils.writeLines(file, projects);
        }

    }


    private static void printAndExportResultsAsCSV (Map<String,Map<StatsKeys,Object>> statsByPackageManager,File csv) throws IOException {

        // print classifications in CSV format
        try (PrintWriter out = new PrintWriter(new FileWriter(csv))) {
            String firstLine = CSV_SEP + Stream.of(StatsKeys.values()).map(c -> c.toString()).collect(Collectors.joining(CSV_SEP));
            out.println(firstLine);
            for (String packageManager : statsByPackageManager.keySet()) {
                out.print(packageManager);
                Map<StatsKeys, Object> classifications = statsByPackageManager.get(packageManager);
                for (StatsKeys key : StatsKeys.values()) {
                    out.print(CSV_SEP);
                    Object value = classifications.get(key);
                    String s = null;
                    if (value instanceof Integer) {
                        s = INT_FORMAT.format((Integer) value);
                    } else if (value instanceof Double) {
                        s = DOUBLE_FORMAT.format((Double) value);
                    } else {
                        throw new IllegalStateException();
                    }
                    out.print(s);
                }
                out.println();
            }
            LOGGER.info("Results in CSV format exported to " + csv.getAbsolutePath());
        }
    }

    private static void printAndExportResultsAsLatex (Map<String,Map<StatsKeys,Object>> statsByPackageManager,File latex,String[] headers, String format,String label,String caption,StatsKeys[] keys) throws IOException {
        // print classifications in tex format
        try (PrintWriter out = new PrintWriter(new FileWriter(latex))) {

            out.println("\\caption{" + caption + "}");
            out.println("\\label{" + label + "}");

            // first line
            out.print("\\begin{tabular}{" + format + "}");
            out.println("\\hline");

            // second line
            out.print(Stream.of(headers).collect(Collectors.joining(" & ")));
            out.println(" \\\\");
            out.println("\\hline");

            for (String packageManager:statsByPackageManager.keySet()) {

                Map<StatsKeys,Object> values = statsByPackageManager.get(packageManager);
                out.print(packageManager);

                for (StatsKeys key:keys) {

                    Object value = values.get(key);
                    assert value!=null;
                    out.print(" & ");
                    if (value instanceof Integer) {
                        out.print(INT_FORMAT.format(value));
                    }
                    else if (value instanceof Double) {
                        out.print(DOUBLE_FORMAT.format((Double)value));
                    }
                    else {
                        LOGGER.warn("adding non-numerical value to latex output");
                        out.print(value);
                    }

                }

                out.println(" \\\\");
            }

            // last line(s)
            out.println("\\hline");
            out.println("\\end{tabular}");

            LOGGER.info("Results in latex format exported to " + latex.getAbsolutePath());
        }

    }

    private static Map<String,String[]> loadFirstAndLast(String pm) throws IOException {
        Map<String,String[]> map = new HashMap<>();
        Preconditions.checkState(FIRST_LAST_DATA_FOLDER.exists());
        File csv = new File(FIRST_LAST_DATA_FOLDER,"first-and-last-" + pm + ".csv");
        Preconditions.checkState(csv.exists());
        List<String> lines = FileUtils.readLines(csv, Charset.defaultCharset().name());
        boolean firstLine = true;
        for (String line:lines) {
            if (firstLine) {
                firstLine = false;
            }
            else {
                String[] tokens = line.split(CSV_SEP);
                assert tokens.length == 5;
                map.put(tokens[1], new String[]{tokens[2], tokens[3], tokens[4]});
            }
        }
        return map;

    }



    private static boolean log(int c) {
        if (c<1_000 && c%100==0) return true;
        else if (c<10_000 && c%1_000==0) return true;
        else if (c<100_000 && c%10_000==0) return true;
        else return (c%100_000==0);
    }


}
