package libiostudy.tests;

import libiostudy.ExtractFirstAndLastVersion;
import libiostudy.Version;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class VersionTests {

    private void lt(String v1,String v2) throws Exception {
        assertTrue(Version.lessThan(v1,v2));
    }

    @Test
    public void testRegular() throws Exception {
        lt("1","2");
        lt("11","12");
        lt("9","11");
        lt("1.1","1.2");
        lt("1.11","1.12");
        lt("1.9","1.11");
        lt("1.2.1","1.2.2");
        lt("1.2.11","1.2.12");
        lt("1.2.9","1.2.11");
    }

    @Test
    public void testRegularWithVPrefix() throws Exception {
        lt("v1","v2");
        lt("v11","v12");
        lt("v9","v11");
        lt("v1.1","v1.2");
        lt("v1.11","v1.12");
        lt("v1.9","v1.11");
        lt("v1.2.1","v1.2.2");
        lt("v1.2.11","v1.2.12");
        lt("v1.2.9","v1.2.11");
    }

    @Test
    public void testDiffSize() throws Exception {
        lt("1","1.2");
        lt("11","11.2");
        lt("1.2","1.2.3");
    }

    @Test
    public void testAdditionalInfo() throws Exception {
        lt("1-alpha","1-beta");
        lt("1-alpha","1.1-alpha");
        lt("1-alpha","1");
    }

    // 0.1.0	0.1.0-rc3
    @Test
    public void testAdditionalInfo2() throws Exception {
        lt("0.1.0-rc3","0.1.0");
    }
}
