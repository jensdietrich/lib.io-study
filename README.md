This repository contains scripts and (intermediate) datasets that have been used and produced in the research leading up to the paper:

*Jens Dietrich, David Pearce, Kelly Blincoe, Jacob Stringer and Amjed Tahir: Dependency Versioning in the Wild. Mining Software Repositories (MSR) 2019.*

Data analysis is based on the data from a [libraries.io snapshot](https://zenodo.org/record/1196312/files/Libraries.io-open-data-1.2.0.tar.gz) loaded into a local Postgres database. The methodology section of the paper and the code comments provide further details.

