package libiostudy;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import com.google.common.math.Stats;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

;

/**
 * Compute stats how dependencies change between first and last version.
 * Generates local stats: looking into the existence of used packages and changes in how
 * dependencies of this package are declared.
 * Queries need an index for this script to be sufficiently fast, the index can be generated using:
 * create index PROJECTVERSION on dependencies(platform,projectname,versionnumber);
 * @author jens dietrich
 */
public class GenerateEvolutionStats2 {

    public static Logger LOGGER = Logging.getLogger("compute-evolution-stats");
    public static File FIRST_LAST_DATA_FOLDER = new File("out/");
    public static final File RESULT_FOLDER = new File("out/");
    public static final File STATS_CSV = new File(RESULT_FOLDER,"evolution-stats-details.csv");
    public static final File STATS_LATEX = new File(RESULT_FOLDER,"evolution-stats-details.tex");
    public static final String CSV_SEP = "\t";
    public static final DecimalFormat DOUBLE_FORMAT = new DecimalFormat("###.##");
    public static final NumberFormat INT_FORMAT = new DecimalFormat("#,###,###");

    public static enum StatsKeys {
        PROJECTS,
        PROJECTS_WITH_DEPENDENCIES_CHANGING_VERSIONING,
        DEPENDENCIES_IN_ADJACENT_VERSIONS,
        DEPENDENCIES_CHANGING_VERSIONS,
	FLEXI2FIXED,FIXED2FLEXI,SEMVER2FIXED,FIXED2SEMVER,FLEXI2SEMVER,SEMVER2FLEXI,FIXED2FIXED,FLEXI2FLEXI,SEMVER2SEMVER
    }

    public static void main (String[] args) throws Exception {

        long t1 = System.currentTimeMillis();

        String tmp = null;
        if (args.length==1) {
            tmp = args[0];
            Preconditions.checkArgument(PackageManagers.ALL.contains(tmp),"This is not the name of a package manager: " + tmp);
            LOGGER.info("Restricting classification to package manager " + tmp);
        }
        final String pckManager = tmp;
        Set<String> packageManagers = pckManager==null ? PackageManagers.ALL : Sets.newHashSet(pckManager);

        // read rules
        File ruleFolder = new File(new File(System.getProperty("user.dir")).getParentFile(),"mapping-rules");
        Preconditions.checkState(ruleFolder.exists());

        RuleParser.LOGGER.setLevel(Level.WARN);
        ClassificationAggregationParser.LOGGER.setLevel(Level.WARN);

        // prep database query
        Properties dbProperties = DBSettings.loadDBSettings();
        Class.forName(dbProperties.getProperty("driver"));
        String url = dbProperties.getProperty("url");
	
        Map<String,Map<StatsKeys,Object>> statsByPackageManager = new ConcurrentHashMap<>();
        AtomicInteger count = new AtomicInteger(0);
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for (String packageManager:packageManagers) {
            Runnable task = () -> {
                Map<StatsKeys,Object> result = analyse(url, ruleFolder, packageManager, count);
                statsByPackageManager.put(packageManager,result);
            };
            executorService.submit(task);
        }
        executorService.shutdown();
        executorService.awaitTermination(24, TimeUnit.HOURS);
        LOGGER.info("Done reading data");
        long t2 = System.currentTimeMillis();
        LOGGER.info("Done, this took " + (t2-t1) + " ms");


        LOGGER.info("Exporting stats");
        printAndExportResultsAsCSV(statsByPackageManager,STATS_CSV);
        printAndExportResultsAsLatex(statsByPackageManager,STATS_LATEX,
				     new String[]{"","PROJ","MOD","CHDEP","ADJDEP","FX2FL","FL2FX","FX2SV","SV2FX","FL2SV","SV2FL","FX2FX","FL2FL","SV2SV"},
            "|lll|rr|rr|rr|rr|",
            "tab:evolution:details",
            "Dependency version strategy changes by package manager (PROJ - overall project count, MOD - projects with at least one dependency changing versioning strategy, " +
                    "CHDEP - changes to the version of a dependency, " +
                    "ADJDEP - number of dependency to same package in adjacent versions, " +
                    "FX2FL - dependencies changing from fixed to flexible, FL2FX - dependencies changing from flexible to fixed," +
                    "FX2SV - dependencies changing from fixed to semantic versioning - style, SV2FX - dependencies changing from semantic versioning - style to fixed, " +
                    "FL2SV - dependencies changing from flexible to semantic versioning - style, SV2FL - dependencies changing from semantic versioning - style to flexible," +
		    "FX2FX - Fixed dependencies that do not change," +
		    "FL2FL - Flexi dependencies that do not change," +
                    "SV2SV - Semver dependencies that do not change."
                ,
				     new StatsKeys[]{StatsKeys.PROJECTS, StatsKeys.PROJECTS_WITH_DEPENDENCIES_CHANGING_VERSIONING, StatsKeys.FIXED2FLEXI, StatsKeys.FLEXI2FIXED, StatsKeys.FIXED2SEMVER, StatsKeys.SEMVER2FIXED, StatsKeys.FLEXI2SEMVER, StatsKeys.SEMVER2FLEXI, StatsKeys.FIXED2FIXED, StatsKeys.FLEXI2FLEXI, StatsKeys.SEMVER2SEMVER}
        );


    }


    private static Map<StatsKeys,Object> analyse(String url,File ruleFolder,String packageManager,AtomicInteger globalCount) {
        LOGGER.info("Processing " + packageManager);
        try (Connection connection = DriverManager.getConnection(url)) {
            List<Rule> classificationRules = RunClassification.parseRules(ruleFolder, packageManager);
            Map<String, String> aggregationRules = RunClassification.parseAggregationRules(ruleFolder);
            // load first-last file
            PreparedStatement stmntProjects = connection.prepareStatement("select distinct projectname from dependencies where platform=?");
            PreparedStatement stmntVersions = connection.prepareStatement("select versionnumber, dependencyname,dependencyrequirements from dependencies where platform=? and projectname=?");

            Map<StatsKeys, Object> stats = new HashMap<>();
            stmntProjects.setString(1, packageManager);

            int projectsChangingStrategy = 0;
            int dependenciesChangingVersion = 0;
            int dependenciesToSamePackageInAdjacentVersions = 0;
            int fixed2flexi = 0;
            int flexi2fixed = 0;
            int fixed2semver = 0;
            int semver2fixed = 0;
            int flexi2semver = 0;
            int semver2flexi = 0;
	    int fixed2fixed = 0;
	    int flexi2flexi = 0;
	    int semver2semver = 0;

            List<String> projects = selectIntoList(stmntProjects, 1);
            LOGGER.info("Processing " + packageManager + " with " + projects.size() + " projects");
            for (String project : projects) {
                if (log(globalCount.incrementAndGet())) {
                    LOGGER.info("Processing project " + globalCount.get());
                }
                boolean projectChangingStrategy = false;
                // version -> (dependency,dependencyversion)
                // Map<String,Map<String,String>> dependenciesByVersion = new TreeMap<>((v1, v2) -> Version.create(v1).compareTo(Version.create(v2)));
                Map<String, Map<String, String>> dependenciesByVersion = new HashMap<>();
                stmntVersions.setString(1, packageManager);
                stmntVersions.setString(2, project);

                ResultSet rs = stmntVersions.executeQuery();
                while (rs.next()) {
                    String version = rs.getString(1);
                    String dependency = rs.getString(2);
                    String dependencyVersion = rs.getString(3);
                    Map<String, String> dependencyVersions = dependenciesByVersion.computeIfAbsent(version, v -> new HashMap<>());
                    dependencyVersions.put(dependency, dependencyVersion);
                }

                Map<String, String> dependencyVersions1 = null;
                Map<String, String> dependencyVersions2 = null;

                List<String> sortedVersions = dependenciesByVersion.keySet().stream().sorted((v1, v2) -> Version.create(v1).compareTo(Version.create(v2))).collect(Collectors.toList());
                for (String version : sortedVersions) {
                    dependencyVersions2 = dependencyVersions1;
                    dependencyVersions1 = dependenciesByVersion.get(version);
                    if (dependencyVersions2 != null) {
                        assert dependencyVersions1 != null;
                        Set<String> dependencies = Sets.intersection(dependencyVersions1.keySet(), dependencyVersions2.keySet());
                        dependenciesToSamePackageInAdjacentVersions = dependenciesToSamePackageInAdjacentVersions + dependencies.size();
                        for (String dependency : dependencies) {
                            String version1 = dependencyVersions1.get(dependency);
                            String version2 = dependencyVersions2.get(dependency);
			    String classification1 = RunClassification.doClassify(version1, classificationRules);
			    String category1 = aggregationRules.get(classification1);
                            if (!version1.equals(version2)) {
                                dependenciesChangingVersion = dependenciesChangingVersion+1;

                                String classification2 = RunClassification.doClassify(version2, classificationRules);
                                String category2 = aggregationRules.get(classification2);

                                if (!category1.equals(category2) && !category1.equals("OTHER") && !category2.equals("OTHER")) {
                                    if (category1.equals("FLEXIBLE") && category2.equals("FIXED")) {
                                        flexi2fixed = flexi2fixed + 1;
                                    } else if (category1.equals("FIXED") && category2.equals("FLEXIBLE")) {
                                        fixed2flexi = fixed2flexi + 1;
                                    } else if (category1.equals("FIXED") && category2.equals("SEMVER")) {
                                        fixed2semver = fixed2semver + 1;
                                    } else if (category1.equals("SEMVER") && category2.equals("FIXED")) {
                                        semver2fixed = semver2fixed + 1;
                                    } else if (category1.equals("FLEXIBLE") && category2.equals("SEMVER")) {
                                        flexi2semver = flexi2semver + 1;
                                    } else if (category1.equals("SEMVER") && category2.equals("FLEXIBLE")) {
                                        semver2flexi = semver2flexi + 1;
                                    }

                                    projectChangingStrategy = true;
                                } else if (category1.equals(category2)){
				    if (category1.equals("FLEXIBLE")){
					flexi2flexi = flexi2flexi + 1;
				    } else if (category1.equals("FIXED")){
					fixed2fixed = fixed2fixed + 1;
				    } else if (category1.equals("SEMVER")){
					semver2semver = semver2semver + 1;
				    }
				}
                            } 
                        }
                    }

                }
                if (projectChangingStrategy) {
                    projectsChangingStrategy = projectsChangingStrategy + 1;
                }
            }

            stats.put(StatsKeys.PROJECTS,projects.size());
            stats.put(StatsKeys.PROJECTS_WITH_DEPENDENCIES_CHANGING_VERSIONING,projectsChangingStrategy);
            stats.put(StatsKeys.DEPENDENCIES_CHANGING_VERSIONS,dependenciesChangingVersion);
            stats.put(StatsKeys.DEPENDENCIES_IN_ADJACENT_VERSIONS,dependenciesToSamePackageInAdjacentVersions);
            stats.put(StatsKeys.FIXED2FLEXI,fixed2flexi);
            stats.put(StatsKeys.FLEXI2FIXED,flexi2fixed);
            stats.put(StatsKeys.FIXED2SEMVER,fixed2semver);
            stats.put(StatsKeys.SEMVER2FIXED,semver2fixed);
            stats.put(StatsKeys.FLEXI2SEMVER,flexi2semver);
            stats.put(StatsKeys.SEMVER2FLEXI,semver2flexi);
	    stats.put(StatsKeys.FIXED2FIXED,fixed2fixed);
	    stats.put(StatsKeys.FLEXI2FLEXI,flexi2flexi);
	    stats.put(StatsKeys.SEMVER2SEMVER,semver2semver);

            LOGGER.info("Done processing data for " + packageManager);
            return stats;
        }
        catch (SQLException x) {
            LOGGER.error("Exception connecting to DB",x);
            System.exit(1);
        }
        return null;

    }

    private static List<String> selectIntoList(PreparedStatement stmnt, int col) throws SQLException {
        List<String> results = new ArrayList<>();
        ResultSet rs = stmnt.executeQuery();
        while (rs.next()) {
            results.add(rs.getString(col));
        }
        return results;
    }

    private static Map<String,String> selectIntoMap(PreparedStatement stmnt, int keyCol, int valCol) throws SQLException {
        Map<String,String> results = new HashMap<>();
        ResultSet rs = stmnt.executeQuery();
        while (rs.next()) {
            results.put(rs.getString(keyCol),rs.getString(valCol));
        }
        return results;
    }

    private static void exportProjectList(List<String> projects, File file) throws IOException {
        if (!projects.isEmpty()) {
            Collections.sort(projects);
            FileUtils.writeLines(file, projects);
        }

    }


    private static void printAndExportResultsAsCSV (Map<String,Map<StatsKeys,Object>> statsByPackageManager,File csv) throws IOException {

        // print classifications in CSV format
        try (PrintWriter out = new PrintWriter(new FileWriter(csv))) {
            String firstLine = CSV_SEP + Stream.of(StatsKeys.values()).map(c -> c.toString()).collect(Collectors.joining(CSV_SEP));
            out.println(firstLine);
            for (String packageManager : statsByPackageManager.keySet()) {
                out.print(packageManager);
                Map<StatsKeys, Object> classifications = statsByPackageManager.get(packageManager);
                for (StatsKeys key : StatsKeys.values()) {
                    out.print(CSV_SEP);
                    Object value = classifications.get(key);
                    String s = null;
                    if (value instanceof Integer) {
                        s = INT_FORMAT.format((Integer) value);
                    } else if (value instanceof Double) {
                        s = DOUBLE_FORMAT.format((Double) value);
                    } else {
                        throw new IllegalStateException("Unexpected value type: " + value + " , " + value.getClass());
                    }
                    out.print(s);
                }
                out.println();
            }
            LOGGER.info("Results in CSV format exported to " + csv.getAbsolutePath());
        }
    }

    private static void printAndExportResultsAsLatex (Map<String,Map<StatsKeys,Object>> statsByPackageManager,File latex,String[] headers, String format,String label,String caption,StatsKeys[] keys) throws IOException {
        // print classifications in tex format
        try (PrintWriter out = new PrintWriter(new FileWriter(latex))) {

            out.println("\\caption{" + caption + "}");
            out.println("\\label{" + label + "}");

            // first line
            out.print("\\begin{tabular}{" + format + "}");
            out.println("\\hline");

            // second line
            out.print(Stream.of(headers).collect(Collectors.joining(" & ")));
            out.println(" \\\\");
            out.println("\\hline");

            for (String packageManager:statsByPackageManager.keySet()) {

                Map<StatsKeys,Object> values = statsByPackageManager.get(packageManager);
                out.print(packageManager);

                for (StatsKeys key:keys) {

                    Object value = values.get(key);
                    assert value!=null;
                    out.print(" & ");
                    if (value instanceof Integer) {
                        out.print(INT_FORMAT.format(value));
                    }
                    else if (value instanceof Double) {
                        out.print(DOUBLE_FORMAT.format((Double)value));
                    }
                    else {
                        LOGGER.warn("adding non-numerical value to latex output");
                        out.print(value);
                    }

                }

                out.println(" \\\\");
            }

            // last line(s)
            out.println("\\hline");
            out.println("\\end{tabular}");

            LOGGER.info("Results in latex format exported to " + latex.getAbsolutePath());
        }

    }

    private static Map<String,String[]> loadFirstAndLast(String pm) throws IOException {
        Map<String,String[]> map = new HashMap<>();
        Preconditions.checkState(FIRST_LAST_DATA_FOLDER.exists());
        File csv = new File(FIRST_LAST_DATA_FOLDER,"first-and-last-" + pm + ".csv");
        Preconditions.checkState(csv.exists());
        List<String> lines = FileUtils.readLines(csv, Charset.defaultCharset().name());
        boolean firstLine = true;
        for (String line:lines) {
            if (firstLine) {
                firstLine = false;
            }
            else {
                String[] tokens = line.split(CSV_SEP);
                assert tokens.length == 5;
                map.put(tokens[1], new String[]{tokens[2], tokens[3], tokens[4]});
            }
        }
        return map;

    }



    private static boolean log(int c) {
        if (c<1_000 && c%100==0) return true;
        else if (c<10_000 && c%1_000==0) return true;
        else if (c<100_000 && c%10_000==0) return true;
        else return (c%100_000==0);
    }


}
