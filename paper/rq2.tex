\section{Changing Dependency Versioning Practices as Projects Evolve (RQ2)}
\label{sec:rq2}

In order to answer RQ2, we extracted the first and the last version of each project, which were identified using the methodology described in Section~\ref{sec:methodology:ordering}. We then compared the dependencies declared in the first and last version. We analysed only the projects that had at least two versions, since this allowed us to investigate evolution of declared dependencies over time. Table \ref{tab:evolution:1} shows some metrics, including the number of projects and the number of projects with only one version for each package manager. The majority of projects have more than one version in the data set, with the exception of the projects using Homebrew.

Table \ref{tab:evolution:1} also shows the average number of versions per project and the respective standard deviations. Those numbers indicate that projects are typically represented by large version ranges, with a significant variation between projects.  For instance, there are 33 NPM projects with  1,000 or more versions in the dataset, and a further 2,584 projects with between 100 and 999 versions.  The project with the most versions is \textit{wix-style-react} --- it provides common React UI components,  with 3,550 versions (ranging from 1.0.0 to 1.1.3547). The large version ranges reflect the trend towards shorter, often highly automated build and release cycles.

\begin{table}[t!]
	\caption{Project evolution by package manager (PROJ - project count, ONE - projects with only one version, AVG/STDEV - average / standard deviation of number of versions per project, AVG1 / STDEV1 - average / standard deviation of number of dependencies in first version, AVGL / STDL - average / standard deviation of number of dependencies in last version}
	\label{tab:evolution:1}
        \centering
        \resizebox{\columnwidth}{!}{%
        \input{evolution-stats1}
    }
\end{table}



Finally, Table \ref{tab:evolution:1} compares the number of declared dependencies the first and the last version for each project, computed using the methodology described in Section~\ref{sec:methodology:ordering}.  The data indicates that the number of dependencies significantly increases over time for projects in all package managers except Homebrew, where the number stays constant. If we consider external dependencies as a source of complexity of a system, this confirms Lehmann's first and second law of software evolution: projects evolve and become more complex by doing so~\cite{lehman1980programs}. Note that there is some additional hidden complexity as we only measure direct, not transitive dependencies.

\subsection{Project Level Analysis}
\label{sec:rq2:projectlevel}
First, we examined the dependency strategies at a high-level by considering the strategies used across all dependencies for each project. The results are summarised in Table~\ref{tab:evolution:2}. We report the number of projects that use flexible / semantic version style dependency versioning in the first version, and add or drop those dependency versioning strategies, shown by its presence or absence in the last version. This is based on the aggregated classification scheme discussed in Section~\ref{sec:methodology:aggregation}. 

\begin{table}[t!]
	\caption{Adaptation of flexible dependencies by package manager (PROJ - project count, SEMV1 / FLEX1 - projects using semantic versioning / flexible dependency syntax in first version, SEM+ / SEM- - projects adapting / dropping semantic dependency versioning syntax between first and last version, FLEX+ / FLEX- - projects adapting / dropping flexible dependency syntax between first and last version}
	\label{tab:evolution:2}
    \centering
     \resizebox{\columnwidth}{!}{%
        \input{evolution-stats2}\texttt{}
    }
\end{table}

As shown in Table~\ref{tab:evolution:2}, projects tend to stick to their way of doing things, and generally resist change with very few projects introducing new dependency versioning strategies or completely removing existing strategies. When projects do change their strategies, they more often move towards using semantic versioning or otherwise flexible dependency declarations, although there are exceptions (notably, \textit{Maven}). 

\subsection{Individual Dependency Level Analysis}
\label{sec:rq2:dependencylevel}
To complement this coarse, project-level analysis, we also analysed how individual dependencies change over time as we hypothesized that projects will change their versioning practice for some, but not all, of their dependencies. 

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{Rplot.pdf}
\vspace{-0.3cm}
\caption{Changes in Dependency Declarations from First to Last Version \label{fig:evolution:details}}
\end{figure}

Again, we found no general trend towards or away from flexible or semantic-versioning style dependency versioning. Once a project chooses a dependency strategy for a particular dependency, it is very unlikely that they will change that strategy. Figure \ref{fig:evolution:details} shows the strategy changes every time a dependency version declaration is changed. As can be seen, nearly 90\% of dependency declaration changes keep the same dependency strategy. This shows that it is very important for projects to consider the implications of these decisions when adding a new dependency.

\subsection{Developer Perspective}
\label{sec:rq2:survey}
%The survey results provide some insights why developers change their practice. 
Interestingly, the number of survey respondents who report to have changed their dependency declaration approach was rather high (42\%). This seems to contradict the results discussed above, we attribute this to the nature of survey participants -- we believe that most participants had an over-average level of experience and interest in managing dependencies.
Although changes in both directions were reported, changes towards the use of fixed versions were more common \footnote{In the survey, we have only asked ``Has your approach to declaring dependencies changed over time?'', and respondents had the option to provide additional text to elaborate, so the above statement is based on these comments}.

Developers who reported to move away from flexible dependency versioning commonly cited concerns about build stability and the introduction of compatibility-related bugs. The following quotes are illustrative of these concerns:~\footnote{Each quote is followed by some summary data about the respective respondent: years of experience, package managers used, and level of familiarity with semantic versioning}: \textit{``Have been burned too many times by so-called point releases on NPM. ''} [10 - 20 years experience, uses NPM, NuGet, Pypi], \textit{``First, realizing that without fixed versions things can break, second change was when I started using cargo which introduced lockfile''} [10 - 20 years experience, uses Cargo, Pypi, very familiar with semantic versioning], \textit{``Taking end-to-end responsibility for software conception, development, and deployment, requires predictable outcomes. If you do not use fixed versions, then rebuilding an artifact to resolve an issue identified during QA testing can cause unrelated changes that can manifest in production.''} [10 - 20 years, uses Homebrew, Maven, very familiar with semantic versioning], \textit{``You begin to realize how sloppy upstream people are, and the issues it causes, so you get a bit better about it.''} [10 - 20 years, uses Homebrew, NPM, NuGet, Pypi, Rubygems, very familiar with semantic versioning],\textit{``I used to declare version ranges, then I realized that even version ranges cannot capture the full ``compatibility range'' of a dependency .. ''} [10 - 20 years, uses NPM, Nix, very familiar with semantic versioning].


Some experienced developers report differences between package managers, and adapt a horses-for-courses approach \textit{``As much as Python packages claim to implement semantic versioning, they always find a way to break something in a minor release. No issues with Rust though.''} [5 - 10 years, uses Cargo, Pypi, very familiar with semantic versioning]. \textit{``Usually due to package maintainers not conforming 100\% to semantic versioning resulting in them shipping code that isn't compatible with the previous version. This is most noticeable on npm; ..''} [10 - 20 years, uses NPM, Packagist, very familiar with semantic versioning]. 

This is sometimes also extended to the usage context, with a more conservative approach taken in commercial projects: \textit{``in serious production code, always fixed version... in open source code, more relaxed''} [20+ years, uses Homebrew, Maven, NPM, Rubygems, very familiar with semantic versioning], and organisation imposing rules: \textit{``At work, some packages we are told to keep under a certain range because it is more supported.''} [2 - 5 years, uses Homebrew, Maven, NPM, not familiar with semantic versioning].

There were also cases where developers reported their rationale for changing towards flexible dependency versioning practices: \textit{``Because while exact versions give you predictability, they're difficult to keep up to date in manually when you have a lot of dependencies (particularly with pip; pipenv improves on that).''} [10 - 20 years, uses NPM, Pypi, very familiar with semantic versioning], \textit{``Used to be fixed, but .. I wanted to keep up to date and using version ranges helped to do that automatically.''} [5 - 10 years, uses	NPM, NuGet, Pypi, Others], \textit{``At the start, it felt the easiest to just use a library and keep the fixed version. However, it ended up being quite limiting (especially when there's 'so many cool new features' that I couldn't use). I therefore prefer accepting a certain range of versions to keep the software "fresh" for a longer time.''} [5 - 10 years, uses CRAN, NuGet, not familiar with semantic versioning].
\\ \\
\noindent\fbox{%
	\parbox{\columnwidth}{%
		\textbf{RQ2 Do projects change their approach as they evolve?}
		\\
		The number of dependencies increases as projects develop across all package managers investigated except for Homebrew, where it stays constant.  Projects both adapt and drop flexible and semantic version-style dependency version declarations, although the number of projects changing strategy is relatively small. Feedback from experienced developers in the survey suggest that they often move away from flexible versioning as they have experienced compatibility-related errors, and value stable (reproducible) builds.  
	}%
}

