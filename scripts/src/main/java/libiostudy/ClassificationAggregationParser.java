package libiostudy;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parses aggregation classifications.
 * Returns a map associating classifications (used in rule heads) with the an aggregated category.
 * @author jens dietrich
 */
public class ClassificationAggregationParser {

    public static Logger LOGGER = Logging.getLogger("aggregationparser");

    public Map<String,String> parse (File aggregationDefs) throws IOException {

        LOGGER.info("parsing aggregation rules from " + aggregationDefs.getAbsolutePath());
        Map<String,String> rules = new HashMap<>();
        List<String> lines = FileUtils.readLines(aggregationDefs, Charset.defaultCharset());

        for (int i=0;i<lines.size();i++) {
            String line = lines.get(i).trim();
            int lineNo = i + 1;

            if (line.startsWith("#") || line.length() == 0) {
                // comment or empty line
            } else if (line.contains("=")) {
                int idx = line.indexOf("=");
                String aggregated = line.substring(0,idx).trim();
                String classificationsDef = line.substring(idx+1);
                String[] classifications = classificationsDef.split("\\|");
                for (String cl:classifications) {
                    cl = cl.trim();
                    if (!Classifications.ALL.contains(cl)) {
                        throw new IllegalStateException("Unknown classification encountered in aggregation rule in line " + lineNo + ": " + cl);
                    }
                    else {
                        rules.put(cl,aggregated);
                        LOGGER.info("Adding aggregation rule " + cl + " -> " + aggregated);
                    }
                }
            }
            else {
                throw new IllegalStateException("Invalid syntax found in line " + lineNo + " -- this is neither a comment nor a valid aggregation rule");
            }
        }

        return rules;
    }


}
