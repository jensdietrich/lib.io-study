package libiostudy;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Check which atom projects are also in NPM
 * @author jens dietrich
 */
public class CheckAtomRecords {

    public static final int SAMPLE_SIZE = 100;
    public static final File SAMPLE_FILE = new File("out/atom-projects.csv");

    static class VersionedProject {
        String name = null;
        String version = null;

        public VersionedProject(String name, String version) {
            this.name = name;
            this.version = version;
        }

        @Override
        public String toString() {
            return "" + name + " - " + version ;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VersionedProject that = (VersionedProject) o;

            if (!name.equals(that.name)) return false;
            return version.equals(that.version);
        }

        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 31 * result + version.hashCode();
            return result;
        }
    }


    public static void main (String[] args) throws Exception {


        // (1) gather Atom projects
        Properties dbProperties = DBSettings.loadDBSettings();
        Class.forName(dbProperties.getProperty("driver"));
        String url = dbProperties.getProperty("url");
        Connection connection = DriverManager.getConnection(url);
        String sql = "select distinct(projectname) as name from dependencies where platform=\'Atom\'";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        Set<String> projectNames = new HashSet<>();
        while (rs.next()) {
            String name = rs.getString("name");
            projectNames.add(name);
        }

        System.out.println("" + projectNames.size() + " Atom projects found");

        // (2) collect projects also found in NPM
        PreparedStatement pStmnt = connection.prepareStatement("select * from dependencies where platform=\'NPM\' and projectname=?");
        Set<String> projectsFoundInNPM = new HashSet<>();
        for (String projetcName:projectNames) {
            if (hasRecord(pStmnt,projetcName)) {
                projectsFoundInNPM.add(projetcName);
            }
        }
        System.out.println("" + projectsFoundInNPM.size() + " projects found in NPM");


        // (3) gather Atom versioned projects
        sql = "select projectname,versionnumber from dependencies where platform=\'Atom\'";
        stmt = connection.createStatement();
        rs = stmt.executeQuery(sql);
        Multimap<String,VersionedProject> projectVersions = HashMultimap.create();
        while (rs.next()) {
            String name = rs.getString("projectname");
            String version = rs.getString("versionnumber");
            VersionedProject projectVersion = new VersionedProject(name,version);
            projectVersions.put(name,projectVersion);
        }
        System.out.println("" + projectVersions.size() + " Atom project versions found");

        // (4) collect project versions also found in NPM
        pStmnt = connection.prepareStatement("select * from dependencies where platform=\'NPM\' and projectname=? and versionnumber=?");
        Multimap<String,VersionedProject> projectVersionsFoundInNPM = HashMultimap.create();
        for (VersionedProject projectVersion:projectVersions.values()) {
            if (hasRecord(pStmnt,projectVersion.name,projectVersion.version)) {
                projectVersionsFoundInNPM.put(projectVersion.name,projectVersion);
            }
        }
        System.out.println("" + projectVersionsFoundInNPM.size() + " project versions found in NPM");

        // (5) collect random subset
        Collection<String> randomProjects = RandomUtils.getRandomSubset(projectVersionsFoundInNPM.keySet(),SAMPLE_SIZE);
        Collection<VersionedProject> randomProjectVersions = new HashSet<>();
        for (String project:randomProjects) {
            VersionedProject projectVersion = projectVersionsFoundInNPM.get(project).iterator().next();
            randomProjectVersions.add(projectVersion);
        }

        // (6) export
        List<String> lines = randomProjectVersions.stream().map(pv -> pv.name + "\t" + pv.version).collect(Collectors.toList());
        FileUtils.writeLines(SAMPLE_FILE,lines);
        System.out.println("Atom project version samples that may overlap with NPM exported to " + SAMPLE_FILE.getAbsolutePath());


    }

    private  static boolean hasRecord(PreparedStatement pStmnt,String... params) throws Exception {
        for (int i=0;i<params.length;i++) {
            pStmnt.setString(i+1,params[i]);
        }
        ResultSet rs = pStmnt.executeQuery();
        boolean result = rs.next();
        rs.close();
        return result;
    }

}
