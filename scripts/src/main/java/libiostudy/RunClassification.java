package libiostudy;

import com.google.common.base.Preconditions;
import libiostudy.util.SplitWideLatexTable;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import java.io.*;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Run the classification. Takes one parameter .. the package manager to be used. This is much faster to run as
 * this will be used in the where clause of the sql query. If missing, all records will be processed with all available rule sets.
 * @author jens dietrich
 */
public class RunClassification {

    public static Logger LOGGER = Logging.getLogger("classifier");

    public static final File UNCLASSIFIED_DEPENDENCIES_FOLDER = new File("out/");
    public static final File RESULT_FOLDER = new File("out/");
    public static final File CLASSIFICATION_LATEX = new File(RESULT_FOLDER,"classification.tex");
    public static final File CLASSIFICATION_LATEX_PART1 = new File(RESULT_FOLDER,"classification-part1.tex");
    public static final File CLASSIFICATION_LATEX_PART2 = new File(RESULT_FOLDER,"classification-part2.tex");
    public static final File CLASSIFICATION_CSV = new File(RESULT_FOLDER,"classification.csv");
    public static final File AGGREGATED_CLASSIFICATION_LATEX = new File(RESULT_FOLDER,"aggregated-classification.tex");
    public static final File AGGREGATED_CLASSIFICATION_CSV = new File(RESULT_FOLDER,"aggregated-classification.csv");
    public static final int UNCLASSIFIED_THRESHOLD = 1_000;
    public static final int PROGRESS_LOG_INTERVAL = 1_000_000;
    public static final String CSV_SEP = "\t";
    public static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#,###,###");
    public static final NumberFormat PERCENTAGE_FORMAT = new DecimalFormat("##.##\\%");
    public static final boolean REPORT_PERCENTAGES = true;
    public static final int QUEUE_SIZE = 5_000_000;
    public static final int CONSUMER_THREADS = Runtime.getRuntime().availableProcessors() - 1;
    public static final int SLOW_CLASSIFICATION_THRESHOLD = 1_000;  // 1s

    public static void main (String[] args) throws Exception {

        long t1 = System.currentTimeMillis();

        File ruleFolder = new File(new File(System.getProperty("user.dir")).getParentFile(),"mapping-rules");
        Preconditions.checkState(ruleFolder.exists());
        Preconditions.checkArgument(args.length<=1,"One or no argument expected");

        List<String> packageManagersWithRules = Stream.of(ruleFolder.list())
                .filter(f -> f.endsWith(".rules"))
                .map(f -> f.substring(0,f.lastIndexOf('.')))
                .sorted()
                .collect(Collectors.toList());

        String tmp = null;
        if (args.length==1) {
            tmp = args[0];
            Preconditions.checkArgument(PackageManagers.ALL.contains(tmp),"This is not the name of a package manager: " + tmp);
            Preconditions.checkArgument(packageManagersWithRules.contains(tmp),"No rule definitions found for: " + tmp);
            LOGGER.info("Restricting classification to package manager " + tmp);
        }
        final String pckManager = tmp;

        Properties dbProperties = DBSettings.loadDBSettings();
        Class.forName(dbProperties.getProperty("driver"));
        String url = dbProperties.getProperty("url");

        Map<String,List<Rule>> rulesByPackageManager = new ConcurrentHashMap<>();
        Map<String,Map<String, Integer>> classificationsByPackageManager = new ConcurrentSkipListMap<>();
        Map<String,List<String>> unclassifiedDependenciesByPackageManager = new ConcurrentHashMap<>();
        BlockingQueue<String[]> queue = new ArrayBlockingQueue<String[]>(QUEUE_SIZE);
        AtomicBoolean QUERY_DONE = new AtomicBoolean(false);

        Runnable producer = () -> {
            try (Connection connection = DriverManager.getConnection(url)) {
                LOGGER.info("Connected to DB");

                String sql = "select platform, dependencyrequirements from dependencies";
                if (pckManager != null) {
                    sql = sql + " where platform=\'" + pckManager + "\'";
                }

                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                LOGGER.info("Received result set");
                int count = 0;
                while (rs.next()) {
                    count = count + 1;
                    if (count % PROGRESS_LOG_INTERVAL == 0) {
                        LOGGER.info("\treading record #" + count + " , queue size: " + queue.size());
                    }
                    String packageManager = rs.getString("platform");
                    if (packageManagersWithRules.contains(packageManager)) {
                        String dependencyrequirements = rs.getString("dependencyrequirements").trim();
                        try {
                            queue.put(new String[]{packageManager,dependencyrequirements});
                        }
                        catch (InterruptedException x) {
                            LOGGER.error("Error putting element in queue",x);
                        }
                    }
                }
                QUERY_DONE.set(true);
                LOGGER.info("Done reading data");
            }
            catch (SQLException x) {
                QUERY_DONE.set(true);
                LOGGER.error("Database error",x);
            }
        };

        AtomicInteger consumerCounter = new AtomicInteger();

        class Consumer implements Runnable  {
            public void run() {
                while (!(QUERY_DONE.get() && queue.isEmpty())) {
                    String[] record = queue.poll();
                    if (record != null) { // null if queue is empty
                        int step = consumerCounter.incrementAndGet();
                        if (step % PROGRESS_LOG_INTERVAL == 0) {
                            LOGGER.info("\tclassifying record #" + step + " , queue size: " + queue.size());
                        }
                        long t1 = System.currentTimeMillis();
                        String packageManager = record[0];
                        String dependencyrequirements = record[1];
                        List<Rule> rules = rulesByPackageManager.computeIfAbsent(packageManager, k -> parseRules(ruleFolder, packageManager));
                        Map<String, Integer> classifications = classificationsByPackageManager.computeIfAbsent(packageManager, k -> new ConcurrentSkipListMap<>());
                        List<String> unclassifiedDependencies = unclassifiedDependenciesByPackageManager.computeIfAbsent(packageManager, k -> Collections.synchronizedList(new ArrayList<>(UNCLASSIFIED_THRESHOLD)));
                        String classification = doClassify(dependencyrequirements, rules);
                        classifications.compute(classification, (k, v) -> (v == null) ? 1 : v + 1);
                        if (classification.equals(Classifications.DEFAULT) && unclassifiedDependencies.size() < UNCLASSIFIED_THRESHOLD) {
                            unclassifiedDependencies.add(dependencyrequirements);
                        }
                        long t2 = System.currentTimeMillis();
                        if ((t2-t1)>SLOW_CLASSIFICATION_THRESHOLD) {
                            LOGGER.warn("Slow classification, package manager: " + packageManager + " , dependency: " + dependencyrequirements + " , time: " + (t2-t1) + " ms");
                        }
                    }
                }
            }
        };

        ExecutorService executor = Executors.newCachedThreadPool();
        executor.submit(producer);
        Stream.generate(() -> new Consumer()).limit(CONSUMER_THREADS).forEach(c -> executor.submit(c));
        executor.shutdown();
        executor.awaitTermination(1,TimeUnit.DAYS);


        LOGGER.info("Exporting sample unclassified records for further analysis");
        for (Map.Entry<String, List<String>> unclassified : unclassifiedDependenciesByPackageManager.entrySet()) {
            File unclassifiedFile = new File(UNCLASSIFIED_DEPENDENCIES_FOLDER, "unclassified-" + unclassified.getKey() + ".txt");
            FileUtils.writeLines(unclassifiedFile, unclassified.getValue());
            LOGGER.info("\t" + unclassified.getValue().size() + " unclassifiable dependencies written to " + unclassifiedFile.getAbsolutePath());
        }
        printAndExportResults(classificationsByPackageManager,CLASSIFICATION_CSV,CLASSIFICATION_LATEX,Classifications.ALL);

        LOGGER.info("Aggregating and exporting classifications");
        Map<String,Map<String,Integer>> aggregatedClassificationsByPackageManager = aggregateClassificationsByPackageManager(ruleFolder,classificationsByPackageManager);
        Collection<String> aggregationCategories = parseAggregationRules(ruleFolder).values().stream().distinct().sorted().collect(Collectors.toList());
        printAndExportResults(aggregatedClassificationsByPackageManager,AGGREGATED_CLASSIFICATION_CSV,AGGREGATED_CLASSIFICATION_LATEX,aggregationCategories);

        long t2 = System.currentTimeMillis();
        LOGGER.info("Done, this took " + (t2-t1) + " ms");


        LOGGER.info("Postprocessing: split wide latex result table");
        SplitWideLatexTable.split(
            CLASSIFICATION_LATEX, CLASSIFICATION_LATEX_PART1, CLASSIFICATION_LATEX_PART2,
            1,7,"{|l|rrrrrrr|}","{|l|rrrrrrr|}");
    }

    static String doClassify(String dependencyrequirements,List<Rule> rules) {
        return classify(dependencyrequirements, rules);
    }

    private static String classify(String dependency, List<Rule> rules) {
        for (Rule rule:rules) {
            if (rule.getMatch().matcher(dependency).matches()) {
                return rule.getClassification();
            }
        }
        return Classifications.DEFAULT;
    }

    static List<Rule> parseRules(File ruleFolder, String packageManager) {
        LOGGER.info("Parsing rules for: " + packageManager);
        try {
            return new RuleParser().parse(new File(ruleFolder, packageManager + ".rules"), true);
        }
        catch (IOException x) {
            LOGGER.fatal("Cannot read rules",x);
            return null;
        }
    }

    static Map<String,String> parseAggregationRules(File ruleFolder) {
        LOGGER.info("Parsing aggregation rules");
        try {
            return new ClassificationAggregationParser().parse(new File(ruleFolder, "aggregation.def"));
        }
        catch (IOException x) {
            LOGGER.fatal("Cannot read aggregation rules",x);
            return null;
        }
    }

    static Map<String,Map<String, Integer>>  aggregateClassificationsByPackageManager(File ruleFolder,Map<String,Map<String, Integer>> classificationsByPackageManager) throws IOException {

        Map<String,String> aggregationRules = parseAggregationRules(ruleFolder);
        Map<String,Map<String, Integer>> aggregated = new LinkedHashMap<>();
        for (String pckMgr:classificationsByPackageManager.keySet()) {
            Map<String,Integer> classificationCounts = classificationsByPackageManager.get(pckMgr);
            Map<String,Integer> aggregationCounts = new HashMap<>();
            for (String classification:classificationCounts.keySet()) {
                int count = classificationCounts.get(classification);
                String aggregatedCategory = aggregationRules.get(classification);
                if (aggregatedCategory==null) {
                    throw new IllegalStateException("Cannot find aggregated category for " + classification);
                }
                aggregationCounts.compute(aggregatedCategory,(k,v) -> v==null?count:count+v);
            }
            aggregated.put(pckMgr,aggregationCounts);

        }
        return aggregated;
    }


    private static void printAndExportResults (Map<String,Map<String, Integer>> classificationsByPackageManager,File csv,File latex,Collection<String> keys) throws IOException {

        LOGGER.info("Results overview");
        for (String packageManager:classificationsByPackageManager.keySet()) {
            LOGGER.info("classification results for " + packageManager);
            int total = classificationsByPackageManager.get(packageManager).values().stream().collect(Collectors.summingInt(Integer::intValue));
            LOGGER.info("\ttotal = " + total);
            for (Map.Entry entry : classificationsByPackageManager.get(packageManager).entrySet()) {
                LOGGER.info("\t" + entry.getKey() + " = " + entry.getValue());
            }
        }

        // print classifications in CSV format
        try (PrintWriter out = new PrintWriter(new FileWriter(csv))) {
            String firstLine = CSV_SEP + keys.stream().collect(Collectors.joining(CSV_SEP));
            out.println(firstLine);
            for (String packageManager:classificationsByPackageManager.keySet()) {
                out.print(packageManager);
                Map<String, Integer> classifications = classificationsByPackageManager.get(packageManager);
                for (String classification:keys) {
                    out.print(CSV_SEP);
                    int value = classifications.getOrDefault(classification,0);
                    out.print(value);
                }
                out.println();
            }
            LOGGER.info("Results in CSV format exported to " + csv.getAbsolutePath());
        }

        // print classifications in tex format
        try (PrintWriter out = new PrintWriter(new FileWriter(latex))) {

            // first line
            out.print("\\begin{tabular}{|l|r");
            for (String classification:keys) {
                out.print('r');
            }
            out.println("|}");
            out.println("\\hline");

            // second line
            out.print(" & total ");
            for (String classification:keys) {
                out.print(" & ");
                out.print(classification);
            }
            out.println(" \\\\");
            out.println("\\hline");

            for (String packageManager:classificationsByPackageManager.keySet()) {
                out.print(packageManager + " & ");
                int total = classificationsByPackageManager.get(packageManager).values().stream().collect(Collectors.summingInt(Integer::intValue));
                out.print(NUMBER_FORMAT.format(total));
                Map<String, Integer> classifications = classificationsByPackageManager.get(packageManager);
                for (String classification:keys) {
                    out.print(" & ");
                    int value = classifications.getOrDefault(classification,0);
                    String v = null;
                    if (REPORT_PERCENTAGES) {
                        double d = ((double)value)/((double)total);
                        v = PERCENTAGE_FORMAT.format(d);
                    }
                    else {
                        v = NUMBER_FORMAT.format(value);
                    }
                    out.print(v);
                }
                out.println(" \\\\");
            }

            // last line(s)
            out.println("\\hline");
            out.println("\\end{tabular}");

            LOGGER.info("Results in latex format exported to " + latex.getAbsolutePath());
        }
        
    }

}
