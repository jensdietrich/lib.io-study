package libiostudy;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Aggregator {
    
    // Type in SQL query and desired column data here
    private static final String delimiter = "\t";
    private static final String SQL = "select platform, projectname, versionnumber, dependencyrequirements from dependencies";
    private static final String[] COLS_FROM_SQL = new String[]{
            "platform",
            "projectname",
            "versionnumber",
            "dependencyrequirements"
    };
    
    // Ignore these
    private static Logger LOGGER = Logging.getLogger("aggregator");
    private static final int CONSUMER_THREADS = Runtime.getRuntime().availableProcessors() - 1;
    private static final int PROGRESS_LOG_INTERVAL = 100_000;
    private static final int QUEUE_SIZE = 2_000_000;
    private static Map<String,List<Rule>> RULES_BY_PACKAGE;
    private static Map<String,Set<String>> DISTINCT_CLASSES_BY_PACKAGE;
    private static Map<String,Map<String,Map<String,Integer>>> RESULTS;
    
    
    public static void main (String[] args) throws Exception {
        // Set up
        getRulesFromFile();

        // Producer gets results from DB and adds them to a Queue for consumption
        BlockingQueue<Map<String, String>> queue = new ArrayBlockingQueue<>(QUEUE_SIZE);
        AtomicBoolean QUERY_DONE = new AtomicBoolean(false);
        Runnable producer = () -> {
            try (Connection connection = getConnection()) {
                LOGGER.info("Connected to DB");

                // Get data from the DB
                connection.setAutoCommit(false);
                LOGGER.info("Querying result set");
                Statement stmt = connection.createStatement();
                stmt.setFetchSize(1000);
                ResultSet rs = stmt.executeQuery(SQL);
                LOGGER.info("Received result set");
                int count = 0;
                while (rs.next()) {
                    count++;

                    if (count % PROGRESS_LOG_INTERVAL == 0) {
                        LOGGER.info("\treading record #" + count + " , queue size: " + queue.size());
                    }

                    // Send results into blocking queue for consumption
                    Map<String, String> res = new HashMap<>();
                    for (String query: COLS_FROM_SQL) {
                        res.put(query, rs.getString(query).trim());
                    }
                    try {
                        queue.put(res);
                    }
                    catch (InterruptedException x) {
                        LOGGER.error("Error putting element in queue",x);
                    }
                }
                connection.setAutoCommit(true);
                stmt.close();

//                // Get one package manager's information at a time. NuGet is particularly slow so reverse order processes it sooner
//                for (String platform: RULES_BY_PACKAGE.keySet().stream().sorted(Collections.reverseOrder()).collect(Collectors.toList())) {
////                    if (!platform.equals("Homebrew"))
////                        continue;
//
//
//
//                }

                QUERY_DONE.set(true);
                LOGGER.info("Done reading data");
            }
            catch (Exception x) {
                QUERY_DONE.set(true);
                LOGGER.error("Database error",x);
            }
        };

        // This consumer class aggregates dependency types by PM
        AtomicInteger consumerCounter = new AtomicInteger();
        class ConsumerCountTypesByPM implements Runnable  {
            public void run() {
                while (!(QUERY_DONE.get() && queue.isEmpty())) {
                    Map<String, String> record = queue.poll();
                    if (record != null) { // null if queue is empty
                        int step = consumerCounter.incrementAndGet();
                        if (step % PROGRESS_LOG_INTERVAL == 0) {
                            LOGGER.info("\tclassifying record #" + step + " , queue size: " + queue.size());
                        }

                        // Here is where the work occurs
                        String platform = record.get("platform");
                        String classification = classify(platform, record.get("dependencyrequirements").trim());
                        DISTINCT_CLASSES_BY_PACKAGE.get(platform).add(classification);
                    }
                }
            }
        };

        // Begin producer and consumers
        LOGGER.info(String.format("There are %d consumer threads available for this programme.", CONSUMER_THREADS));
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.submit(producer);
        Stream.generate(() -> new ConsumerCountTypesByPM()).limit(CONSUMER_THREADS).forEach(c -> executor.submit(c));
        executor.shutdown();
        executor.awaitTermination(1,TimeUnit.DAYS);
        
        // Save data
        try {
            List<String> classes = new ArrayList<>();
            classes.add("Total");
            Map<String, String> absclass = getAbstractClassifications(classes);

            Map<String, Integer[]> results = new HashMap<>();
            for (Map.Entry<String, Set<String>> obj: DISTINCT_CLASSES_BY_PACKAGE.entrySet()) {
                Integer[] collate = new Integer[classes.size()];
                for (int i = 0; i < collate.length; i++) collate[i] = new Integer(0);
                obj.getValue().forEach(v -> {
                    collate[0]++;
                    collate[classes.indexOf(absclass.get(v))]++;
                });
                results.put(obj.getKey(), collate);
            }

            saveClassificationsByPM(classes, results, new File("../data/classification/dependenciesbyPM.csv"));
            printAndExportResultsAsLatex(classes, results, new File("../data/classification/dependenciesbyPM.tex"));
        } catch (IOException e) {
            LOGGER.error("Error writing results", e);
        }
        
    }

    // Customise output of classifications by Package Manager. Uses DISTINCT_CLASSES_BY_PACKAGE.
    private static void saveClassificationsByPM (List<String> headers, Map<String, Integer[]> results, File path) throws IOException {
        PrintWriter output = new PrintWriter(new FileWriter(path));

        // Titles
        output.println("sep="+delimiter);
        output.print("Platform");
        headers.forEach(abstractclassification -> output.print(delimiter + abstractclassification));
        output.println();

        // Write results
        results.entrySet().stream().map(Map.Entry::getKey).sorted().forEach(k -> {
            output.print(k);
            Arrays.stream(results.get(k)).forEach(value -> output.print(delimiter + value));
            output.println();
        });

        LOGGER.info("Results in csv format exported to " + path.getAbsolutePath());
        output.close();
    }

    private static void printAndExportResultsAsLatex (List<String> headers, Map<String, Integer[]> results, File path) throws IOException {
        // print classifications in tex format
        PrintWriter out = new PrintWriter(new FileWriter(path));

        out.println("\\documentclass{article}");
        out.println("\\begin{document}");
        out.println("\\label{Dependency Types Used}");
        out.println("\\begin{table}");

        // first line
        out.print("\\begin{tabular}{|l|r|rrrr|}");
        out.println("\\hline");

        // second line
        out.print(" & ");
        out.print(String.join(" & ", headers));
        out.println(" \\\\");
        out.println("\\hline");


        // Write results
        results.entrySet().stream().map(Map.Entry::getKey).sorted().forEach(k -> {
            out.print(k);
            Arrays.stream(results.get(k)).forEach(value -> out.print(" & " + value));
            out.println(" \\\\");
        });

        // last line(s)
        out.println("\\hline");
        out.println("\\end{tabular}");
        out.println("\\end{table}");
        out.println("\\end{document}");

        LOGGER.info("Results in latex format exported to " + path.getAbsolutePath());
        out.close();

    }

    // Uses mapping-rules/aggregation.def to map full classification set to more abstract groupings
    private static Map<String, String> getAbstractClassifications(List<String> classes) {
        Map<String, String> abstracts = new HashMap<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(new File("../mapping-rules/aggregation.def")));

            String line = in.readLine();
            while(line != null) {
                String[] equals = line.split(" = ");
                classes.add(equals[0]);
                for (String classification: equals[1].split(" \\| ")) {
                    abstracts.put(classification, equals[0]);
                }
                line = in.readLine();
            }
        } catch (IOException e) {
            LOGGER.error("Error reading abstract mappings", e);
        }

        return abstracts;
    }
    
//    public static void saveClassificationsByVersion () throws IOException {
//        output = new BufferedWriter(new FileWriter(new File("../data/depsbyversion.csv")));
//        Collection<String> classByOrder = Classifications.ALL;
//        output.write("sep=" + delimiter + "\n");
//        output.write("Platform" + delimiter + "Project" + delimiter + "Version" + delimiter);
//        for (String i: classByOrder)
//            output.write(i + delimiter);
//
//        output.write("\n");
//        for (Map.Entry<String, Map<String, Map<String, Integer>>> pairplat: RESULTS.entrySet()) {
//            for(Map.Entry<String, Map<String, Integer>> pairvers: pairplat.getValue().entrySet()) {
//                if (pairvers.getValue().size() > 0) {
//                    output.write(pairplat.getKey() + delimiter);
//                    output.write(pairvers.getKey() + delimiter);
//                    for (String k: classByOrder) {
//                        output.write((pairvers.getValue().get(k) == null) ? ("0" + delimiter) : (pairvers.getValue().get(k)+delimiter));
//                    }
//                    output.write("\n");
//                }
//            }
//        }
//        output.close();
//    }
    

    
//    // This consumer class aggregates dependencies by project
//    static class ConsumerGetAllClassifications implements Runnable  {
//        public void run() {
//            while (!(QUERY_DONE.get() && queue.isEmpty())) {
//                Map<String, String> record = queue.poll();
//                if (record != null) { // null if queue is empty
//                    int step = consumerCounter.incrementAndGet();
//                    if (step % PROGRESS_LOG_INTERVAL == 0) {
//                        LOGGER.info("\tclassifying record #" + step + " , queue size: " + queue.size());
//                    }
//
//                    // Here is where the work occurs
//                    String platform = record.get("platform");
//                    String classification = classify(platform, record.get("dependencyrequirements"));
//                    DISTINCT_CLASSES_BY_PACKAGE.get(platform).add(classification);
//
//                    // Add to project(delimiter)version results
//                    String projectversionpair = record.get("projectname") + delimiter + record.get("versionnumber");
//                    RESULTS.get(platform).putIfAbsent(projectversionpair, new ConcurrentHashMap<String, Integer>());
//                    Map<String, Integer> projectVersionRes = RESULTS.get(platform).get(projectversionpair);
//                    projectVersionRes.compute(classification, (k,v) -> (v == null) ? 1 : v+1);
//                }
//            }
//        }
//    }

    private static String classify(String platform, String dependency) {
        for (Rule rule: RULES_BY_PACKAGE.get(platform)) {
            if (rule.getMatch().matcher(dependency).matches()) {
                return rule.getClassification();
            }
        }
        return Classifications.DEFAULT;
    }
    
    private static List<Rule> parseRules(File ruleFolder, String packageManager) {
        LOGGER.info("Parsing rules for: " + packageManager);
        try {
            return new RuleParser().parse(new File(ruleFolder, packageManager + ".rules"), true);
        }
        catch (IOException x) {
            LOGGER.fatal("Cannot read rules",x);
            return null;
        }
    }

    public static void getRulesFromFile() {
        // Get all rules available
        File ruleFolder = new File(new File(System.getProperty("user.dir")).getParentFile(),"mapping-rules");
        Preconditions.checkState(ruleFolder.exists());
        List<String> packageManagersWithRules = Stream.of(ruleFolder.list())
                .filter(f -> f.endsWith(".rules"))
                .map(f -> f.substring(0,f.lastIndexOf('.')))
                .sorted()
                .collect(Collectors.toList());

        RULES_BY_PACKAGE = new ConcurrentHashMap<>();
        DISTINCT_CLASSES_BY_PACKAGE = new ConcurrentHashMap<>();
        RESULTS = new ConcurrentHashMap<>();
        for (String packagemanager : packageManagersWithRules) {
            RULES_BY_PACKAGE.put(packagemanager, parseRules(ruleFolder, packagemanager));
            DISTINCT_CLASSES_BY_PACKAGE.put(packagemanager, Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>()));
            RESULTS.put(packagemanager, new ConcurrentHashMap<>());
        }

    }
    
    public static Connection getConnection() throws Exception {
        Properties dbProperties = loadDBSettings();
        Class.forName(dbProperties.getProperty("driver"));
        String url = dbProperties.getProperty("url");      
        // String user = dbProperties.getProperty("user");
        // String pwd = dbProperties.getProperty("pwd");
        return DriverManager.getConnection(url, dbProperties);
    }
    
    private static Properties loadDBSettings() throws Exception  {
        String dbConfigFileName = "db.properties";
        Preconditions.checkState(new File(dbConfigFileName).exists(),"db configuration file not found, create file using thre followiong template:\n" +
                "driver=org.postgresql.Driver\n"+
                "url=jdbc:postgresql://localhost:5432/lib.io\n"+
                "url=jdbc:sqlite:/local/scratch/dependencies.db\n" +

                "sqlite example: \n" +
                "driver=org.sqlite.JDBC\n" +
                "url=jdbc:sqlite:/local/scratch/dependencies.db");
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(dbConfigFileName)) {
            properties.load(reader);
        }
        Preconditions.checkState(properties.containsKey("driver"));
        Preconditions.checkState(properties.containsKey("url"));
        return properties;
    }
}