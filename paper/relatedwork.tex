\section{Related Work}

\subsection{Evolution of Software Ecosystems}

Gonzalez-Barahona et al. ~\cite{gonzalez2009macro} studied the growth of the Debian Linux distribution. They found that the average size of packages remained stable, while the overall size of the distribution has been doubling every 2 years and the number of dependencies increased exponentially. This is consistent with some of our findings in Section~\ref{sec:rq2} on how the number of dependencies increased.

Several authors investigated the tooling aspect of precisely extracting and representing dependencies, this includes the work of Lungu et. al for Smalltalk~\cite{lungu2010recovering}, German et. al. work on \cite{german2007model} Debian Linux. Men's et al. looked for evidence of Lehmann's laws of software evolution in the Eclipse ecosystem~\cite{mens2008evolution}, and found that Eclipse bundles adhere to the laws of continuing change and growth.

Espinha et al. studied web APIs~\cite{espinha2014web} and found a general lack of standards and adherence to best practices. This confirms our finding that practices significantly differ between ecosystems. 

%\subsection{Studies of  Dependencies}

Bavota et al. have studied dependencies between 147 Java  Apache projects, covering  1,964 releases over 14 years \cite{bavota2015apache}. They studied which changes triggered dependency upgrades (bug fixes trigger upgrades, while changes to interfaces make upgrades less likely), and reported that changes between versions have generally only a limited impact on clients. The authors do not precisely model the dependency resolution mechanism used by the actual systems. An interesting finding is that projects are selective when upgrading dependencies, i.e., if they upgrade, they do not perform all available upgrades.

Bogart et al. studied dependency practices in the CRAN, Eclipse and NPM ecosystems \cite{bogart2016break}. They found that practices differ significantly between ecosystems, all communities invest in tooling to facilitate the maintenance of dependencies, and policies change over time. We include those ecosystems in our work~\footnote{Although we do not study the Eclipse ecosystem  directly, we note that many Java libraries that are part of the Maven ecosystem are also released as OSGi bundles}, and the results by Bogart et al. confirm our findings that different ecosystems develop unique characteristics.

Decan et al. studied the topology of dependency graphs in seven ecosystems (which were all included in our study): Cargo, CPAN, CRAN, NPM, NuGet, Packagist, and RubyGems~\cite{decan2018empirical}.  This study is based on the libraries.io dataset, but does not systematically study how dependencies are versioned.  Thus, it is complementary to the work presented here.

Kikas et al. studied dependencies in the JavaScript, Ruby, and Rust ecosystems~\cite{kikas2017structure}, and found reveal significant differences across those systems. We make a similar observation with respect to versioning practices in our study.

%\cite{kula2018developers}\cite{kikas2017structure} \cite{jenson2010empirical}
%\cite{abdalkareem2017developers}

%Amjed: new work on how to use metrics to help developers decide which library to use \cite{delaMora2018}
%Jens: does not look too relevant

\subsection{Problems Caused by Evolution}

Robbes et al. studied the ripple effect caused by  APIs changes in the Smalltalk ecosystem~\cite{robbes2012developers}. They found that API deprecation can have a significant impact on the ecosystem in terms of broken dependencies that need fixing. Sawant et al. studied the API deprecation mechanism in Java~\cite{sawant2018understanding} from the point of view of API producers and consumers and found several shortcomings.

%Jens: no space to expand this further

Dietrich et al. uses static analysis to study the impact of problems arising from subtle binary incompatibility issues that can break client applications which are simply relinked, rather than being recompiled~\cite{dietrich2014broken}. They found evidence that breaking changes are common, although this rarely results in issues affecting actual clients. A separate survey by the same authors of 414 developers revealed a possible cause: even experienced developers have only limited understanding of the rules of binary compatibility, and this leads to a large number of compatibility issues reported and discussed by developers online. Xavier et al. obtained similar results, also studying Java~\cite{xavier2017historical}. Raemaekers et al. work is conceptually similar, they studied evolution on the Maven ecosystem~\cite{raemaekers2017semantic}.   One of their research questions was whether the adherence to semantic versioning principles has increased over time. In order to answer this question, they observed breaking changes in non-major releases and observed a moderate decrease from 28.4\% in 2006 to 23.7\% in 2011, indicating that developers are becoming more aware of semantic versioning.

Dietrich et al. also looked into semantic changes that effect compatibility~\cite{dietrich2017contracts}.  By harvesting contracts from code, they identified semantic changes using evolution data from Java programs in the Maven repository. For instance, the strengthening of a precondition when an API evolves is considered as an incompatible change that may break clients.

Linares-V{\'a}squez et al. studied the impact of the use of fault- and change-prone APIs has on the success of Android applications, and found that it negatively impacted the success of apps~\cite{linares2013api}. Derr et al. also study library dependencies in Android apps, and found large number of outdated versions of libraries being used that could be easily upgraded, in many cases the versions used have known vulnerabilities~\cite{derr2017keep}.

Pashchenko et al. studied the impact of vulnerabilities in open-source libraries and, to get industrial relevance, selected libraries used in commercial SAP products~\cite{Pashchenko2018Vulnerable}. They found that most (but not all) vulnerabilities can be fixed by simply updating dependencies, which highlights the importance of versioning schemes that facilitate safe, automated dependency resolution. Kula et al. conducted a similar study on GitHub projects using Maven dependencies, and found that 81.5\% of the  systems studied kept  outdated dependencies, and developers are often unaware of vulnerabilities in libraries they depend on~\cite{kuladevelopers}.

\subsection{Tool Support for Dependency Management}


%Jens: no space for further details
There are several studies on how to migrate client code to adapt to changing dependencies, including the work of Cossette and Walker~\cite{cossette2012seeking} and Dig and Johnson~\cite{dig2006apis} to classify API changes and to automatically recommend and generate refactorings. Henkel and Diwan~\cite{henkel2005catchup} propose catchup, a tool to record the refactorings used to change APIs, and use those recorings to adapt clients. In all those papers, smaller sets of Java programs were studied. 

Jezek et al. reviewed and benchmarked several API compatibility checkers~\cite{jezek2017api}, focusing on Java source and binary compatibility issues studied in their previous work~\cite{dietrich2014broken}. They also suggested a modified Java compiler that produced byte code that avoids some binary incompatibility issues~\cite{jezek2016magic}. This automates the refactoring process suggested by other authors ~\cite{cossette2012seeking},\cite{dig2006apis} for certain types of compatibilities. 

Foo et al. report on an API incompatibility checker that uses static analysis~\cite{ foo2018efficient}. A statically constructed callgraph is used to detect deep changes that can effect compatibility. The tool works for the Maven Central, PyPI, and RubyGems
ecosystems, and the authors report that based on the experiments with the tools,  26\% of library versions are in violation of semantic versioning. The analysis suffers from the imprecision of the static analysis being used (VTA). 


\subsection{Summary}

There is a large body of work on software ecosystems and evolution, and any discussion of selected work has to be selective. We notice the following main points: 

\begin{enumerate}
	\item Empirical studies often use Java (usually Maven, older studies often use Eclipse), some of the other systems we are interested in are either under-represented, or ignored altogether. The only other study we are aware of that tries to capture practices across a wide range of package managers is \cite{decan2018empirical}.
	\item There is plenty of evidence in existing work that both upgrading and not upgrading dependencies can have an adverse effect through compatibility issues and not-addressed  faults in dependencies, respectively. 
	\item There is ongoing research in how to provision better tools to facilitate dependency management.
	
\end{enumerate}
