package libiostudy;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Extract first and last versions, and export them to files.
 * @author jens dietrich
 */
public class ExtractFirstAndLastVersion {

    public static Logger LOGGER = Logging.getLogger("extract-first-last");
    public static final File RESULT_FOLDER = new File("out/");
    public static final String CSV_SEP = "\t";

    public static void main (String[] args) throws Exception {

        long t1 = System.currentTimeMillis();

        String tmp = null;
        if (args.length==1) {
            tmp = args[0];
            Preconditions.checkArgument(PackageManagers.ALL.contains(tmp),"This is not the name of a package manager: " + tmp);
            LOGGER.info("Restricting classification to package manager " + tmp);
        }
        final String pckManager = tmp;
        Set<String> packageManagers = pckManager==null ? PackageManagers.ALL : Sets.newHashSet(pckManager);

        Properties dbProperties = DBSettings.loadDBSettings();
        Class.forName(dbProperties.getProperty("driver"));
        String url = dbProperties.getProperty("url");

        try (Connection connection = DriverManager.getConnection(url)) {
            LOGGER.info("Connected to DB");
            AtomicInteger count = new AtomicInteger(0);
            for (String pm:packageManagers) {

                LOGGER.info("Processing " + pm);

                String sql = "select distinct versionnumber, projectname from dependencies";
                if (pm != null) {
                    sql = sql + " where platform=\'" + pm + "\'";
                }

                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                Map<String,String> firstVersions = new HashMap<>();
                Map<String,String> lastVersions = new HashMap<>();
                Map<String,Integer>  versionCounts = new HashMap<>();

                while (rs.next()) {
                    String projectName = rs.getString("projectname").trim();
                    String version = rs.getString("versionnumber").trim();

                    int step = count.incrementAndGet();
                    if (log(step)) {
                        LOGGER.info("\tprocessing record #" + step);
                    }

                    firstVersions.compute(projectName, (k,v) -> v==null?version:(Version.lessThan(v,version)?v:version));
                    lastVersions.compute(projectName, (k,v) -> v==null?version:(Version.lessThan(v,version))?version:v);
                    versionCounts.compute(projectName, (k,v) -> v==null?1:v+1);
                }

                exportStats(pm,firstVersions,lastVersions,versionCounts);

                LOGGER.info("Done processing data for " + pm);

                // export
            }
            LOGGER.info("Done reading data");
        }
        catch (SQLException x) {
            LOGGER.error("Database error",x);
        }
        long t2 = System.currentTimeMillis();
        LOGGER.info("Done, this took " + (t2-t1) + " ms");
    };

    private static void exportStats(String packageManager,Map<String,String> firstVersions,Map<String,String> lastVersions,Map<String,Integer> versionCounts) throws Exception {
        File file = new File(RESULT_FOLDER,"first-and-last-" + packageManager + ".csv");
        try (PrintWriter out = new PrintWriter(new FileWriter(file))) {
            String firstLine = "package manager " + CSV_SEP + "project" + CSV_SEP + "first-version" + CSV_SEP + "last-version" + CSV_SEP + "version-count";
            out.println(firstLine);

            Set<String> projects = firstVersions.keySet().stream().sorted().collect(Collectors.toSet());

            for (String project:projects) {
                String firstVersion = firstVersions.get(project);
                String lastVersion = lastVersions.get(project);
                assert lastVersion!=null;
                int count = versionCounts.get(project);
                out.print(packageManager);
                out.print(CSV_SEP);
                out.print(project);
                out.print(CSV_SEP);
                out.print(firstVersion);
                out.print(CSV_SEP);
                out.print(lastVersion);
                out.print(CSV_SEP);
                out.print(count);
                out.println();
            }

            LOGGER.info("Results for " + packageManager + " in CSV format exported to " + file.getAbsolutePath());
        }
    }

    private static boolean log(int c) {
//        if (c<1_000 && c%100==0) return true;
//        else if (c<10_000 && c%1_000==0) return true;
//        else if (c<100_000 && c%10_000==0) return true;
//        else return (c%100_000==0);

        return (c%100_000==0);
    }





}
