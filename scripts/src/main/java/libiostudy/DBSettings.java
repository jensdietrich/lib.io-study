package libiostudy;

import com.google.common.base.Preconditions;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

/**
 * Load and provide access to DB settings.
 * @author jens dietrich
 */
public class DBSettings {

    static Properties loadDBSettings() throws Exception  {
        String dbConfigFileName = "db.properties";
        Preconditions.checkState(new File(dbConfigFileName).exists(),"db configuration file not found, create file using the following template:\ndb configuration file not found, create file using thr following template:\n" +

                "driver=org.postgresql.Driver\n"+
                "url=jdbc:postgresql://localhost:5432/lib.io\n"+
                "url=jdbc:sqlite:/local/scratch/dependencies.db\n" +

                "sqlite example: \n" +
                "driver=org.sqlite.JDBC\n" +
                "url=jdbc:sqlite:/local/scratch/dependencies.db");
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(dbConfigFileName)) {
            properties.load(reader);
        }
        Preconditions.checkState(properties.containsKey("driver"));
        Preconditions.checkState(properties.containsKey("url"));
        return properties;
    }
}
